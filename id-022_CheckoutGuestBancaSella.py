#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-022_CheckoutGuestBancaSella.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_checkout_guest_banca_sella():

    driver.switch_to.window(driver.window_handles[0])

    #prendo i dati per effettuare la registrazione dal configFile
    email = config.get('SEZIONE_GUEST', 'email_guest')
    nome =  config.get('SEZIONE_GUEST', 'nome_guest')
    cognome = config.get('SEZIONE_GUEST', 'cognome_guest')
    telefono = config.get('SEZIONE_GUEST', 'telefono_guest')
    url = config.get('SEZIONE_GUEST', 'url_guest')

    time.sleep(2)

    prodotto = str(random.randint(1, 1))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div[' + prodotto + ']/a/img').click()

    try:
        driver.implicitly_wait(10)
        element = WebDriverWait(driver, 20).until(
            EC.presence_of_all_elements_located((By.ID, 'option-label-lunghezza-152'))
        )
        driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-4"]').click()
        print("Ho selezionato una lunghezza")
        prezzo = driver.find_element_by_css_selector("span.price").text
        driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    finally:
        time.sleep(2)
        print("Ho aggiunto un prodotto al carrello")

    print("Dopo aver aggiunto un prodotto al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[2]/div[2]/div/div/div/div[1]').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vai al Checkout'")
    driver.find_element_by_xpath('//*[@id="top-cart-btn-checkout"]').click()
    time.sleep(5)

    try:
        assert "CHECKOUT" in driver.find_element_by_xpath('//*[@id="checkout"]/div[1]/h1').text
        print("Sono al Checkout")
    finally:
        time.sleep(3)

    print("Inserisco i dati per l'ordine")
    time.sleep(3)
    driver.find_element_by_id("customer-email").click()
    driver.find_element_by_id("customer-email").clear()
    driver.find_element_by_id("customer-email").send_keys(email)
    print("Ho inserito l'email")

    driver.find_element_by_name('firstname').click()
    driver.find_element_by_name('firstname').send_keys(nome)
    print("Ho inserito il nome")

    driver.find_element_by_name('lastname').click()
    driver.find_element_by_name('lastname').send_keys(cognome)
    print("Ho inserito il cognome")

    driver.find_element_by_name('street[0]').click()
    driver.find_element_by_name('street[0]').send_keys("Pedro Alvarez Cabrai, 16")
    print("Ho inserito l'indirizzo")

    driver.find_element_by_name('region').click()
    Select(driver.find_element_by_name("region")).select_by_visible_text("Cosenza")
    print("Ho selezionato la provincia")

    driver.find_element_by_name('city').click()
    Select(driver.find_element_by_name("city")).select_by_visible_text("Rende")
    print("Ho selezionato la città")

    driver.find_element_by_name('postcode').click()
    Select(driver.find_element_by_name("postcode")).select_by_visible_text("87036")
    print("Ho selezionato il CAP")

    driver.find_element_by_name('telephone').click()
    driver.find_element_by_name('telephone').send_keys(telefono)
    print("Ho inserito il numero di telefono")

    # driver.find_element_by_xpath("//div[9]/div/input").click()
    # driver.find_element_by_xpath("//div[9]/div/input").send_keys("FTTTST80A01C351A")
    # print("Ho inserito il codice fiscale")
    #
    # p_iva = random.randint(00000000001, 99999999999)
    #
    # driver.find_element_by_name("vat_id").click()
    # driver.find_element_by_name("vat_id").clear()
    # driver.find_element_by_name("vat_id").send_keys(p_iva)
    # print("Ho inserito la Partita Iva")
    # time.sleep(3)

    print("Seleziono un metodo di pagamento")
    driver.find_element_by_xpath("//div[@id='checkout-payment-method-load']/div/div/div[3]/div/label").click()
    print("Ho selezionato 'Banca Sella'")
    time.sleep(5)

    print("Inserisco il numero della carta")
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").clear()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").send_keys("4775718800002026")

    print("Inserisco il mese di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]')).select_by_visible_text("05 - maggio")

    print("Inserisco l'anno di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]')).select_by_visible_text("2027")

    print("Inserisco il codice CVV")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').click()
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').send_keys("646")

    print("Inserisco il nome cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").send_keys("Mario Rossi")

    print("Inserisco l'email cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").send_keys("mariorossi@future.com")

    print("Inserisco un commento")
    driver.find_element_by_id("order_comments").click()
    driver.find_element_by_id("order_comments").send_keys("Test prova")
    time.sleep(3)

    print("Sto per effettuare l'ordine")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(5)

    print("Vado a controllare che l'ordine sia stato concluso con successo")
    try:
        assert "Grazie per aver effettuato il tuo acquisto !!!" in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/h2').text
        print("Ordine effettuato con successo")
    finally:
        time.sleep(3)

    n_ordine = driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/p[1]/span').text

    second_driver.switch_to_window(second_driver.window_handles[-1])
    time.sleep(5)
    second_driver.get(url)

    try:
        print("Sto per aprire l'email della conferma d'ordine")
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div.all_message-min_text.all_message-min_text-3")))
        second_driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
    finally:
        print("Ho aperto la mail della conferma ordine")

    second_driver.switch_to.frame("msg_body")
    time.sleep(3)

    ordine_email = second_driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/h1/span').text

    n_ordine_email = ordine_email [1:]

    if (n_ordine == n_ordine_email):
        print("Email conferma ordine ricevuta correttamente")

# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_checkout_guest_banca_sella()
driver.quit()
second_driver.quit()
print("END " + nomeTest)