#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-018_PasswordDimenticata.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_password_dimenticata():

    driver.switch_to.window(driver.window_handles[0])

    email = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')
    url_reg = config.get('SEZIONE_REGISTRAZIONE' , 'urlgen')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="login-form"]/fieldset/div[4]/div[2]/a').click()
    print("Ho cliccato su 'Hai dimenticato la password?'")

    try:
        assert "Hai Dimenticato la Password" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div/h1/span").text
        print("Box per il recupero password caricato correttamente")
    finally:
        time.sleep(5)

    driver.find_element_by_id("email_address").click()
    driver.find_element_by_id("email_address").send_keys(email)
    time.sleep(5)
    print("Ho inserito l'email usata in fase di registrazione")

    print("Sto per cliccare su 'Resetta la mia password'")
    driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
    time.sleep(3)
    print("Ho cliccato su 'Resetta la mia password'")

    second_driver.switch_to_window(second_driver.window_handles[-1])
    second_driver.get(url_reg)

    second_driver.find_element_by_xpath('//*[@id="login"]').click()

    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys(email)
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="f"]/table/tbody/tr[1]/td[3]').click()
    time.sleep(5)

    print("Sto per resettare la password")
    second_driver.switch_to.frame("ifmail")
    time.sleep(5)

    path = "dev"
    url = "https://devenv:mp9bMbQ39@fitt-dev.alkemy-stage.com/"

    url_reset = second_driver.find_element_by_xpath(
        '//*[@id="mailmillieu"]/div[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/a').get_attribute('href')
    print "L'email di conferma è arrivata correttamente"

    driver.switch_to.window(driver.window_handles[0])

    if path == "dev":
        conferma = url_reset[33:]
    driver.get(url + conferma)

    print "Ho aperto la pagina per il reset della password"
    time.sleep(5)

    print("Sto per inserire la nuova password")
    driver.find_element_by_id("password").click()
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(password)

    print("Confermo la nuova password")
    driver.find_element_by_id("password-confirmation").click()
    driver.find_element_by_id("password-confirmation").clear()
    driver.find_element_by_id("password-confirmation").send_keys(password)

    print("Sto per salvare la nuova password")
    driver.find_element_by_css_selector("button.action.submit.primary > span").click()
    time.sleep(5)

    try:
        assert "Hai aggiornato la tua password." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Password aggiornata correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection

Connection().connection(driver)

test_password_dimenticata()
driver.quit()
second_driver.quit()
print("END " + nomeTest)