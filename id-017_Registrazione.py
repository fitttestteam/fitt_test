#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-017_Registrazione.py'
print('START '+nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.implicitly_wait(100)
second_driver.implicitly_wait(100)
driver.delete_all_cookies()
second_driver.delete_all_cookies()
print ("eliminati tutti i cookies")

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

# def mailinatorAccess():
#     print("\nsto per aprire Mailinator")
#     second_driver.get("https://www.mailinator.com/")
#     print("ho aperto Mailinator")
#     try:
#         element = WebDriverWait(second_driver, 50).until(
#             EC.presence_of_element_located((By.XPATH, '//*[@id="inboxfield"]'))
#         )
#         element = WebDriverWait(second_driver, 50).until(
#             EC.presence_of_element_located((By.XPATH, '/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button'))
#         )
#         second_driver.find_element_by_xpath('//*[@id="inboxfield"]').click()
#         casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(4)]
#         casuale = ''.join(casuale_1)
#         emailCasuale = "fitt_"+casuale+"@mailinator.com"
#         second_driver.find_element_by_xpath('//*[@id="inboxfield"]').send_keys(emailCasuale)
#         second_driver.find_element_by_xpath('/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button').click()
#     finally:
#         # driver.quit()
#         print("ho generato la mail "+emailCasuale)
#
#     config.set('SEZIONE_REGISTRAZIONE', 'emailgen', emailCasuale)
#     with open(filediconfigurazione, 'w') as configfile:
#          config.write(configfile)
#
#     #salvo la url di riferimento nel ConfigFile
#     url = second_driver.current_url
#     config.set('SEZIONE_REGISTRAZIONE', 'urlgen', url)
#     with open(filediconfigurazione, 'w') as configfile:
#         config.write(configfile)

def yopmail():
    second_driver.get("http://www.yopmail.com/it/")
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(4)]
    casuale = ''.join(casuale_1)
    emailCasuale = "fitt_"+casuale+"@yopmail.com"
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys(emailCasuale)
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="f"]/table/tbody/tr[1]/td[3]').click()

    # salvo l'email nel ConfigFile
    config.set('SEZIONE_REGISTRAZIONE', 'emailgen', emailCasuale)
    with open(filediconfigurazione, 'w') as configfile:
         config.write(configfile)

    #salvo la url di riferimento nel ConfigFile
    url = second_driver.current_url
    config.set('SEZIONE_REGISTRAZIONE', 'urlgen', url)
    with open(filediconfigurazione, 'w') as configfile:
        config.write(configfile)


def test_registrazione():
    time.sleep(5)

    #prendo i dati per effettuare la registrazione dal configFile
    email = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')

    nome =  config.get('DATI_REGISTRAZIONE', 'nomereg')
    cognome = config.get('DATI_REGISTRAZIONE', 'cognomereg')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    driver.switch_to.window(driver.window_handles[-1])

    time.sleep(5)

    path = "dev"
    url = "https://devenv:mp9bMbQ39@fitt-dev.alkemy-stage.com/"

    url_accesso = driver.find_element_by_xpath('//*[@id="fitt-sticky-header"]/header/div/div[2]/div[1]/ul/li/a').get_attribute('href')
    print(url_accesso)

    # driver.switch_to.window(driver.window_handles[0])

    if path == "dev":
        # conferma = url_conf[74:]
        conferma = url_accesso[34:]
        print(conferma)
        url_def = url + conferma
        print(url_def)

    driver.get(url_def)

    # print("Sto per cliccare su 'Accedi'")
    # driver.find_element_by_link_text("Accedi").click()
    # time.sleep(3)
    print("Sto per cliccare su 'Crea un account'")
    driver.find_element_by_xpath("//div[2]/div/div/a/span").click()
    time.sleep(3)

    print("Ho effettuato l'accesso al form di registrazione")
    driver.find_element_by_id("firstname").click()
    driver.find_element_by_id("firstname").clear()
    driver.find_element_by_id("firstname").send_keys(nome)
    print("Ho inserito il nome")
    driver.find_element_by_id("lastname").click()
    driver.find_element_by_id("lastname").clear()
    driver.find_element_by_id("lastname").send_keys(cognome)
    print("Ho inserito il cognome")
    driver.find_element_by_xpath('//*[@id="is_subscribed"]').click()
    print("Ho messo la spunta su 'Iscriviti alla Newsletter'")
    driver.find_element_by_id("email_address").click()
    driver.find_element_by_id("email_address").clear()
    driver.find_element_by_id("email_address").send_keys(email)
    print("Ho inserito l'indirizzo email")
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(password)
    print("Ho inserito la password")
    driver.find_element_by_id("password-confirmation").click()
    driver.find_element_by_id("password-confirmation").clear()
    driver.find_element_by_id("password-confirmation").send_keys(password)
    print("Ho confermato la password")

    print("Seleziono il consenso al trattamento dei dati personali")
    driver.find_element_by_xpath('//*[@id="global-privacy-field-1-agree-yes"]').click()
    time.sleep(2)

    driver.find_element_by_xpath('//*[@id="global-privacy-field-2-agree-yes"]').click()
    time.sleep(2)

    driver.find_element_by_xpath('//*[@id="form-validate"]/div[1]/div[1]/button').click()
    print("Ho cliccato su 'Crea un Account'")
    time.sleep(3)

    second_driver.switch_to_window(second_driver.window_handles[-1])
    second_driver.refresh()

    print("Sto per confermare account")
    time.sleep(5)
    second_driver.switch_to.frame("ifmail")
    time.sleep(3)

    path = "dev"
    url = "https://devenv:mp9bMbQ39@fitt-dev.alkemy-stage.com/"

    url_conf = second_driver.find_element_by_xpath('//*[@id="mailmillieu"]/div[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/a').get_attribute('href')
    print(url_conf)
    print "L'email di conferma è arrivata correttamente"

    driver.switch_to.window(driver.window_handles[0])

    if path == "dev":
        # conferma = url_conf[74:]
        conferma = url_conf[33:]
        print(conferma)
        url_def = url + conferma
        print(url_def)

    driver.get(url_def)

    print "L'account è stato confermato"
    time.sleep(5)

    try:
        assert "Grazie per esserti registrato con YOYO by FITT IT." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Account Confermato Correttamente")
    finally:
        time.sleep(2)

    second_driver.switch_to_window(second_driver.window_handles[-1])
    time.sleep(3)
    second_driver.refresh()

    second_driver.switch_to.frame("ifinbox")
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="m1"]/div/a/span[2]').click()
    print("Ho aperto l'email di benvenuto")

    second_driver.switch_to.default_content()

    second_driver.switch_to.frame("ifmail")
    time.sleep(3)

    try:
        assert "Grazie per aver confermato il tuo account YOYO by FITT IT." in second_driver.find_element_by_xpath(
        "//div[@id='mailmillieu']/div[2]/table/tbody/tr/td/table/tbody/tr[2]/td/p[2]").text
        print("Email di Benvenuto arrivata correttamente")
    finally:
        time.sleep(2)

    # second_driver.switch_to.default_content()
    #
    # second_driver.switch_to.frame("ifinbox")
    # time.sleep(3)
    #
    # second_driver.find_element_by_xpath('//*[@id="m2"]/div/a/span[2]').click()
    # print("Ho aperto la mail dell'Iscrizione alla newsletter")
    #
    # second_driver.switch_to.default_content()
    #
    # second_driver.switch_to.frame("ifmail")
    # time.sleep(3)
    #
    # try:
    #     assert "Sei stato iscritto alla nostra newsletter." in second_driver.find_element_by_css_selector(
    #         "#mailmillieu > div:nth-child(2) > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td").text
    #     print("Email 'Iscrizione alla Newsletter' ricevuta correttamente")
    # finally:
    #     time.sleep(2)

    driver.switch_to.window(driver.window_handles[0])

    print("Effettuo il logout")
    driver.find_element_by_xpath("//span/span").click()
    driver.find_element_by_link_text("Esci").click()

    try:
        assert "Ti sei disconnesso" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div/h1/span").text
        print("Logout effettuato con successo")
    finally:
        time.sleep(5)

    print("Effettuo un login di prova per controllare il corretto funzionamento della registrazione")
    time.sleep(5)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email utilizzata in fase di registrazione")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    driver.find_element_by_xpath('//*[@id="fitt-sticky-header"]/header/div/div[2]/div[1]/ul/li[1]/span').click()
    # driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="fitt-sticky-header"]/header/div/div[2]/div[1]/ul/li[1]/div/ul/li[1]/a').click()


    dati_account = driver.find_element_by_css_selector("#maincontent > div > div > div.content-inner.col-sm-9 > div.block.block-dashboard-info > div.block-content > div.box.box-information > div.box-content").text
    print(dati_account)
    time.sleep(5)
    account = nome + " " + cognome +"\n" + email

    if(dati_account == account):
        print("Registrazione effettuato correttamente")

    print("Login effettuato con successo")


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

yopmail()
test_registrazione()
driver.quit()
second_driver.quit()
print("\n")
print("Dati di Controllo")
print("\n")
print("URL Email Generata")
print(config.get('SEZIONE_REGISTRAZIONE', 'urlgen'))
print("Indirizzo email di registrazione")
print(config.get('SEZIONE_REGISTRAZIONE', 'emailgen'))
print("Password di registrazione")
print(config.get('DATI_REGISTRAZIONE', 'passwordreg'))
print("\n")
print("END "+nomeTest)