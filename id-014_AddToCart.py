#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-014_AddToCart.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_add_to_cart():
    time.sleep(3)

    carrello = driver.find_element_by_xpath(
        '/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text

    prodotto = str(random.randint(1,3))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()
    time.sleep(3)

    d = 0
    size = [0 for x in range(d)]
    i = 0

    all_option = driver.find_elements_by_tag_name('div')
    for option in all_option:
        if option.get_attribute('class') == 'swatch-option text':
            lung = option.get_attribute('option-id')
            size.append(lung)
            d = d + 0
            i = i + 1

    lunghezza = (len(size))

    a = random.randint(0 , lunghezza-1)
    b = str(size[a])

    driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-' + b +'"]').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    time.sleep(2)
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(3)

    carrello_1 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text
    # print(carrello_1)

    if(carrello < carrello_1):
        print("Prodotto aggiunto al carrello correttamente")

    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[1]/div/a/img').click()

    prodotto = str(random.randint(1, 3))

    print("Sto per cliccare sul prodotto")
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li[' + prodotto + ']/div/div[1]/a[2]/span/span/img').click()
    time.sleep(3)
    print("Ho cliccato sul prodotto e sono nel dettaglio prodotto")

    time.sleep(5)
    driver.switch_to.frame(0)

    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    print("Sono andato alla scheda prodotto")
    time.sleep(5)

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="product-addtocart-button-sticky"]').click()

    print("Ho cliccato su 'Aggiungi al carrello da 'buy button'")
    time.sleep(5)
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(3)

    carrello_2 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text

    if (carrello_1 < carrello_2):
        print("Prodotto aggiunto al carrello correttamente")

    # driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[2]/a/span").click()
    # time.sleep(3)
    # print("Ho cliccato su 'Accessori'")
    #
    # accessore = str(random.randint(1, 6))
    #
    # print("Sto per aggiungere un 'Accessore' al carrello")
    # driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[5]/ol/li[' + accessore + ']/div/div/div[2]/div/div[1]/form/button').click()
    # time.sleep(3)
    # print("Ho aggiunto un 'Accessore' al carrello")
    #
    # carrello_3 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text
    #
    # if (carrello_2 < carrello_3):
    #     print("Prodotto aggiunto al carrello correttamente")



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_add_to_cart()
driver.quit()
print("END "+nomeTest)