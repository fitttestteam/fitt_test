#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import re
import configparser

nomeTest = 'id-002_HeaderLingue.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_header_lingue():
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="switcher-language"]/div/ul/li[2]/a/img').click()
    print("Ho cliccato sul Store Inglese")
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="switcher-language"]/div/ul/li[3]/a/img').click()
    print("Ho cliccato sul Store Francese")
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="switcher-language"]/div/ul/li[4]/a/img').click()
    print("Ho cliccato sul Store Tedesco")
    time.sleep(5)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_header_lingue()
driver.quit()
print("END "+nomeTest)