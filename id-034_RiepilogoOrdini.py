#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-034_RiepilogoOrdini.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_riepilogo_ordini():

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath("//span/span").click()
    driver.find_element_by_link_text("Il mio Account").click()
    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("I Miei ordini").click()
    print("Ho cliccato su 'I Miei ordini' dal menù sulla sinistra")

    try:
        assert "Ordine n." in driver.find_element_by_xpath("//table[@id='my-orders-table']/thead/tr/th").text
        print("Colonna tabella 'Ordine n.' presente")
    finally:
        time.sleep(2)

    try:
        assert "Spedisci A" in driver.find_element_by_xpath("//table[@id='my-orders-table']/thead/tr/th[3]").text
        print("Colonna tabella 'Spedisci A' presente")
    finally:
        time.sleep(2)

    try:
        assert "Azione" in driver.find_element_by_xpath("//table[@id='my-orders-table']/thead/tr/th[6]").text
        print("Colonna tabella 'Azione' presente")
    finally:
        time.sleep(2)

    print("Sto per cliccare su 'Visualizza ordine'")
    driver.find_element_by_xpath("//table[@id='my-orders-table']/tbody/tr/td[6]/a/span").click()
    time.sleep(3)

    try:
        assert "INDIRIZZO DI SPEDIZIONE" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[4]/div[2]/div/strong/span").text
        print("Indirizzo di spedizione presente")
    finally:
        time.sleep(2)

    try:
        assert "INDIRIZZO DI FATTURAZIONE" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[4]/div[2]/div[3]/strong/span").text
        print("Indirizzo di fatturazione presente")
    finally:
        time.sleep(2)

    try:
        assert "Consegna Standard", driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[4]/div[2]/div[2]/div").text
        print("Metodo di spedizione selezionato 'Consegna Standard")
    finally:
        time.sleep(2)

    try:
        assert "METODO DI PAGAMENTO", driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[4]/div[2]/div[4]/strong/span").text
        print("Metodo di pagamento presente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_riepilogo_ordini()
driver.quit()
print("END " + nomeTest)