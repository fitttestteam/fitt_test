#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-019_LoginErrata.py'
print('START '+nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_login_errata():

    email = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email errata")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys("a" + email)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Verifico che esca il messaggio di errore della login")
    try:
        assert u"Non hai eseguito l'accesso correttamente o il tuo account è temporaneamente disattivato." in driver.find_element_by_xpath(
                              "//main[@id='maincontent']/div/div/div/div[2]/div[2]/div/div/div").text
        print("Messaggio uscito correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_login_errata()
driver.quit()
print("END "+nomeTest)