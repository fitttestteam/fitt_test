#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-033_RubricaIndirizzi.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_rubrica_indirizzi():

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath("//span/span").click()
    driver.find_element_by_link_text("Il mio Account").click()
    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Rubrica").click()
    print("Ho cliccato su 'Rubrica' dal menù sulla sinistra")

    try:
       assert "INDIRIZZO DI SPEDIZIONE PREDEFINITO" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[4]/div[2]/div[2]/strong/span").text
       print("Indirizzo inserito al momento della registrazione presente")
    finally:
        time.sleep(3)

    try:
         assert "AGGIUNGI NUOVO INDIRIZZO", driver.find_element_by_xpath("(//button[@type='button'])[3]").text
         print("Bottone 'Aggiungi nuovo indirizzo' presente")
    finally:
        time.sleep(2)

    print("Aggiungo un nuovo indirizzo")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[6]/div[1]/button').click()
    time.sleep(3)

    print("Verifico i campi obbligatori")
    driver.find_element_by_xpath("//form[@id='form-validate']/div/div/button/span").click()

    try:
        assert u"Questo è un campo obbligatorio." in  driver.find_element_by_id("telephone-error").text
        print("Messaggio di errore uscito correttamente su telefono")
    finally:
        time.sleep(2)

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("street_1-error").text
        print("Messaggio di errore uscito correttamente su indirizzo")
    finally:
        time.sleep(2)

    try:
        assert u"Questo è un campo obbligatorio.", driver.find_element_by_id("city-error").text
        print("Messaggio di errore uscito correttamente su Città")
    finally:
        time.sleep(2)

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("zip-error").text
        print("Messaggio di errore uscito correttamente su CAP")
    finally:
        time.sleep(2)


    print("Inserisco il nome")
    driver.find_element_by_id("firstname").click()
    driver.find_element_by_id("firstname").clear()
    driver.find_element_by_id("firstname").send_keys("Giuseppe")

    print("Inserisco il cognome")
    driver.find_element_by_id("lastname").click()
    driver.find_element_by_id("lastname").clear()
    driver.find_element_by_id("lastname").send_keys("Verdi")

    print("Inserisco il numero di telefono")
    driver.find_element_by_id("telephone").click()
    driver.find_element_by_id("telephone").send_keys("3336688999")

    print("Inserisco la via")
    driver.find_element_by_id("street_1").click()
    driver.find_element_by_id("street_1").send_keys("Via F.Fera 88/A")

    print("Inserisco la città")
    driver.find_element_by_id("city").click()
    driver.find_element_by_id("city").send_keys("Spezzano Albanese")

    print("Inserisco il CAP")
    driver.find_element_by_id("zip").click()
    driver.find_element_by_id("zip").send_keys("87019")

    print("Salvo il nuovo indirizzo")
    driver.find_element_by_xpath("//form[@id='form-validate']/div/div/button/span").click()

    try:
        assert "Hai salvato l'indirizzo." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Indirizzo salvato con successo")
    finally:
        time.sleep(2)

    print("Modifico l'indirizzo appena registrato e lo rendo Indirizzo di fatturazione predefinito")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[5]/div[2]/ol/li/div/a[1]').click()
    time.sleep(2)

    print("Seleziono l'indirizzo come indirizzo di fatturazione predefinito")
    driver.find_element_by_id("primary_billing").click()

    print("Salvo le modifiche")
    driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()

    print("Cambio la via all'indirizzo di spedizione predefinito")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div[2]/div[2]/div[2]/a').click()

    print("Inserisco la nuova via")
    driver.find_element_by_id('street_1').click()
    driver.find_element_by_id('street_1').clear()
    driver.find_element_by_id('street_1').send_keys("Via Rossini, 74")

    print("Salvo le modifiche")
    driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()

    try:
        assert "Hai salvato l'indirizzo." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Indirizzo salvato con successo")
    finally:
        time.sleep(2)



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_rubrica_indirizzi()
driver.quit()
print("END " + nomeTest)