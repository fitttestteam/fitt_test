#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-025_CheckoutLoggatoHPSuccess.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_checkout_loggato_hp_success():

    driver.switch_to.window(driver.window_handles[0])

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')
    url_reg = config.get('SEZIONE_REGISTRAZIONE', 'urlgen')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    #prendo i dati per effettuare la registrazione dal configFile
    telefono = config.get('SEZIONE_GUEST', 'telefono_guest')
    time.sleep(2)

    prodotto = str(random.randint(1,3))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent > a > span:nth-child(2)')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()
    time.sleep(3)

    d = 0
    size = [0 for x in range(d)]
    i = 0

    all_option = driver.find_elements_by_tag_name('div')
    for option in all_option:
        if option.get_attribute('class') == 'swatch-option text':
            lung = option.get_attribute('option-id')
            size.append(lung)
            d = d + 0
            i = i + 1

    lunghezza = (len(size))

    a = random.randint(0 , lunghezza-1)
    b = str(size[a])

    driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-' + b +'"]').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    time.sleep(2)
    print("Ho aggiunto un prodotto al carrello")

    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div/a/img').click()

    prodotto = str(random.randint(1, 2))

    print("Sto per cliccare sul prodotto")
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li[' + prodotto + ']/div/div[1]/a[2]/span/span/img').click()
    time.sleep(3)
    print("Ho cliccato sul prodotto e sono nello Store View")

    time.sleep(5)
    driver.switch_to.frame(0)

    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    print("Sono andato alla scheda prodotto")
    time.sleep(5)

    driver.find_element_by_xpath('//*[@id="product_addtocart_form"]/div/div/div[2]').click()
    print("Ho aggiunto un prodotto al carrello dalla 'Scheda prodotto'")
    time.sleep(5)

    print("Dopo aver aggiunto dei prodotti al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[3]/div[1]/div/div/div/div[1]/a').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vai al Checkout'")
    driver.find_element_by_xpath('//*[@id="top-cart-btn-checkout"]').click()
    time.sleep(10)

    try:
        assert "CHECKOUT" in driver.find_element_by_xpath('//*[@id="checkout"]/div[1]/h1').text
        print("Sono al Checkout")
    finally:
        time.sleep(3)

    try:
        assert "INDIRIZZO DI SPEDIZIONE" in driver.find_element_by_id("shipping_step_header").text
        print("Prima colonna caricata")
    finally:
        time.sleep(3)

    try:
        assert "METODI DI SPEDIZIONE" in driver.find_element_by_id("shipping_method_step_header").text
        print("Seconda colonna caricata")
    finally:
        time.sleep(3)

    try:
        assert "RIVEDI ORDINE" in driver.find_element_by_id("review_step_header").text
        print("Terza colonna caricata")
    finally:
        time.sleep(3)

    print("Seleziono un metodo di pagamento")
    driver.find_element_by_xpath("//div[@id='checkout-payment-method-load']/div/div/div[2]/div/label").click()
    time.sleep(3)

    print("Sto per eliminare un prodotto dal carrello")
    driver.find_element_by_xpath("//tr[2]/td[3]/div[2]").click()
    time.sleep(3)
    driver.find_element_by_xpath("/html/body/div[3]/aside[6]/div[2]/footer/button[2]").click()
    time.sleep(5)
    print("Ho eliminato un prodotto dal carrello")

    driver.find_element_by_name('street[0]').click()
    driver.find_element_by_name('street[0]').send_keys("Pedro Alvarez Cabrai, 16")
    print("Ho inserito l'indirizzo")

    driver.find_element_by_name('region').click()
    Select(driver.find_element_by_name("region")).select_by_visible_text("Cosenza")
    print("Ho selezionato la provincia")

    driver.find_element_by_name('city').click()
    Select(driver.find_element_by_name("city")).select_by_visible_text("Rende")
    print("Ho selezionato la città")

    driver.find_element_by_name('postcode').click()
    Select(driver.find_element_by_name("postcode")).select_by_visible_text("87036")
    print("Ho selezionato il CAP")

    driver.find_element_by_name('telephone').click()
    driver.find_element_by_name('telephone').send_keys(telefono)
    print("Ho inserito il numero di telefono")

    items = ['Azienda' , 'Professionista']
    a = random.choice(items)
    print(a)

    driver.find_element_by_xpath("//div[8]/div/div/label").click()
    time.sleep(5)

    if (a == "Azienda"):

        p_iva = random.randint(00000000001, 99999999999)

        Select(driver.find_element_by_xpath("//div[9]/div/select")).select_by_visible_text(a)
        driver.find_element_by_xpath("//div[9]/div/select").click()
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[11]/div/input").click()
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[11]/div/input").clear()
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[11]/div/input").send_keys("Pasquale")
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[12]/div/input").click()
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[12]/div/input").send_keys(p_iva)
        print("Ho inserito la Partita Iva")
    else:
        Select(driver.find_element_by_xpath("//div[9]/div/select")).select_by_visible_text("Professionista")
        driver.find_element_by_xpath("//div[9]/div/select").click()
        driver.find_element_by_xpath("//div[@id='shipping-new-address-form']/div[10]/div/input").send_keys("FTTTST80A01C351A")
        print("Ho inserito il codice fiscale")

    print("Inserisco un commento")
    driver.find_element_by_id("order_comments").click()
    driver.find_element_by_id("order_comments").send_keys("Test prova")
    time.sleep(5)

    print("Sto per effettuare l'ordine")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(5)

    print("Vado a controllare che l'ordine sia stato concluso con successo")
    try:
        assert "Grazie per aver effettuato il tuo acquisto !!!" in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/h2').text
        print("Ordine effettuato con successo")
    finally:
        time.sleep(3)

    n_ordine = driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/p[1]/a/strong').text
    print(n_ordine)

    second_driver.switch_to_window(second_driver.window_handles[-1])
    time.sleep(5)
    second_driver.get(url_reg)

    second_driver.find_element_by_xpath('//*[@id="login"]').click()

    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys(email_1)
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="f"]/table/tbody/tr[1]/td[3]').click()
    time.sleep(5)

    second_driver.switch_to.frame("ifinbox")
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="m1"]/div/a/span[2]').click()
    print("Ho aperto l'email della conferma d'ordine")

    second_driver.switch_to.default_content()

    second_driver.switch_to.frame("ifmail")
    time.sleep(3)

    ordine_email = second_driver.find_element_by_xpath('//*[@id="mailmillieu"]/div[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/h1/span').text

    n_ordine_email = ordine_email [1:]
    print(n_ordine_email)

    if (n_ordine == n_ordine_email):
        print("Email conferma ordine ricevuta correttamente")


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_checkout_loggato_hp_success()
driver.quit()
second_driver.quit()
print("END " + nomeTest)