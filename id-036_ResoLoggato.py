#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-036_ResoLoggato.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_reso_loggato():

    driver.switch_to.window(driver.window_handles[0])

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[3]/div[2]/ul/li[1]/span').click()
    driver.find_element_by_link_text("Il mio Account").click()

    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("I Miei ordini").click()
    print("Ho cliccato su 'I Miei ordini' dal menù sulla sinistra")

    n_ordine = driver.find_element_by_xpath('//*[@id="my-orders-table"]/tbody/tr/td[1]').text
    print(n_ordine)
    time.sleep(5)

    second_driver.switch_to_window(second_driver.window_handles[-1])

    second_driver.delete_all_cookies()
    print ("eliminati tutti i cookies")

    second_driver.implicitly_wait(100)

    url = config.get('SEZIONE_BE', 'url_1')

    print("sto per aprire il BE di Fitt")
    second_driver.get(url)
    print("ho aperto il BE di Fitt")

    print("Sto per inserire le credenziali")
    second_driver.find_element_by_xpath('//*[@id="username"]').click()
    second_driver.find_element_by_xpath('//*[@id="username"]').send_keys("pasquale.piane")
    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys("Fitt2018")
    time.sleep(3)
    second_driver.find_element_by_xpath('//*[@id="login-form"]/fieldset/div[3]/div[1]/button/span').click()
    print("Ho effettuato la login in BE")

    print("Sto cliccando su Sales")
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "SALES"))
        )
        second_driver.find_element_by_link_text("SALES").click()
    finally:
        print("Ho cliccato su Sales")

    print("Sto cliccando su Orders")
    time.sleep(3)
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Orders"))
        )
        second_driver.find_element_by_link_text("Orders").click()
    finally:
        print("Ho cliccato su Orders")

    time.sleep(5)
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[3]/div/button").click()
    time.sleep(5)

    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").click()
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").clear()
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").send_keys(n_ordine)
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/div/div/button[2]/span").click()
    time.sleep(5)

    second_driver.find_element_by_xpath("//div[@id='container']/div/div[4]/table/tbody/tr/td[8]").click()
    print("Ho cliccato sull'ordine effettuato in precedenza")
    time.sleep(5)

    a = second_driver.find_element_by_id("order_status").text
    print(a)

    print("Sto per cliccare su 'Invoice'")
    second_driver.find_element_by_xpath('//*[@id="order_invoice"]/span').click()
    time.sleep(3)

    print("Seleziono l'invio della mail per la creazione della fattura")
    second_driver.find_element_by_id('send_email').click()
    time.sleep(3)

    print("Sto per cliccare su 'Submit Invoice'")

    if (a == "Pending"):
        second_driver.find_element_by_xpath("//div[3]/button/span").click()
    else:
        second_driver.find_element_by_xpath("//div[4]/button/span").click()
    time.sleep(3)

    try:
        assert "The invoice has been created." in second_driver.find_element_by_xpath("//div[@id='messages']/div/div/div").text
        print("Fattura creata correttamente")
    finally:
        time.sleep(3)
        print("Sto per cliccare su 'Ship'")

    second_driver.find_element_by_xpath('//*[@id="order_ship"]/span').click()
    time.sleep(2)
    print("Ho cliccato su 'Ship")

    print("Seleziono l'invio email per la conferma di spedizione")
    # driver2.find_element_by_xpath('//*[@id="ship_items_container"]/section[2]/div[3]/div[2]/div[2]/label').click()
    second_driver.find_element_by_id('send_email').click()
    time.sleep(3)

    print("Sto per cliccare su 'Submit Shipment")
    # driver2.find_element_by_link_text('Submit Shipment').click()
    second_driver.find_element_by_xpath("//div[3]/button/span").click()
    time.sleep(3)

    try:
        assert "The shipment has been created." in second_driver.find_element_by_xpath("//div[@id='messages']/div/div/div").text
        print("Spedizione effettuta con successo")
    finally:
        time.sleep(3)

    driver.switch_to_window(driver.window_handles[-1])
    driver.refresh()

    driver.find_element_by_link_text("Resi").click()
    print("Ho cliccato su 'Resi' dal menù sulla sinistra")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[1]/div/button').click()
    print("Ho cliccato su 'Crea nuovo reso'")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div/table/tbody/tr[1]/td[2]').click()
    # driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div/table/tbody/tr[1]/td[2]')
    print("Ho selezionato l'ordine da rendere")
    time.sleep(10)

    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div/table/tbody/tr[1]/td[7]/button').click()
    print("Ho cliccato vai al passo 'Successivo'")
    time.sleep(2)

    driver.find_element_by_name('custom_fields[1]').click()
    Select(driver.find_element_by_name('custom_fields[1]')).select_by_visible_text("Rimborso")
    print("Ho selezionato la 'Risoluzione'")

    driver.find_element_by_name('custom_fields[2]').click()
    Select(driver.find_element_by_name('custom_fields[2]')).select_by_visible_text("Danneggiato")
    print("Ho selezionato la 'Condizione del prodotto'")

    driver.find_element_by_name("item_selected").click()
    print("Ho selezionato il prodotto da restituire")
    time.sleep(5)

    driver.find_element_by_xpath("//div[3]/div[2]/div/select").click()
    Select(driver.find_element_by_xpath("//div[3]/div[2]/div/select")).select_by_visible_text("Prodotto danneggiato parzialmente")
    print("Ho selezionato la 'Risoluzione'")

    driver.find_element_by_id("message").click()
    driver.find_element_by_id("message").clear()
    driver.find_element_by_id("message").send_keys("Prova")

    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div/form/div/button').click()
    print("Richiesta di reso inviata")
    time.sleep(3)

    try:
        assert u"Il reso è stato creato con successo. A breve riceverai un'email con i dettagli e le istruzioni della richiesta." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Reso creato correttamente")
    finally:
        time.sleep(3)

    prov = driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
    reso = prov[28:]

    second_driver.switch_to_window(second_driver.window_handles[-1])
    second_driver.refresh()

    print("Sto cliccando su Sales")
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "SALES"))
        )
        second_driver.find_element_by_link_text("SALES").click()
    finally:
        print("Ho cliccato su Sales")

    print("Sto cliccando su 'Manage RMA'")
    time.sleep(3)
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Manage RMA"))
        )
        second_driver.find_element_by_link_text("Manage RMA").click()
    finally:
        print("Ho cliccato su 'Manage RMA'")

    print("Filtro i resi con il numero di reso salvato in frontend")
    time.sleep(5)
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[2]/div[1]/div[2]/div/button').click()
    time.sleep(5)

    second_driver.find_element_by_name('increment_id').click()
    second_driver.find_element_by_name('increment_id').clear()
    second_driver.find_element_by_name('increment_id').send_keys(reso)
    time.sleep(3)
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[2]/div[1]/div[4]/div/div/button[2]').click()
    time.sleep(3)

    print("Entro nella pagina del reso per controllare che sia corretto")
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[3]/table/tbody/tr/td[2]/div/a').click()
    time.sleep(5)

    print("Salvo il numero del reso di Backend")
    a = second_driver.find_element_by_xpath("//body[@id='html-body']/div[2]/header/div/div/h1").text
    reso_be = a[16:]
    print(reso_be)

    print("Confronto i due codici per verificae la corretta funzione di reso")
    if(reso == reso_be):
        print("Reso inserito correttamente")

    url_spedito = config.get('SEZIONE_REGISTRAZIONE', 'urlgen')
    second_driver.get(url_spedito)
    email = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    print("Vado a controllare se è arrivata l'email di conferma spedizione")
    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys(email)
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="f"]/table/tbody/tr[1]/td[3]').click()

    second_driver.switch_to.frame("ifinbox")
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="m2"]/div/a/span[2]').click()
    print("Ho aperto l'email di ordine spedito")

    second_driver.switch_to.default_content()

    second_driver.switch_to.frame("ifmail")
    time.sleep(3)

    try:
        assert "Il tuo ordine su YOYO by FITT IT è stato spedito" in second_driver.find_element_by_xpath('//*[@id="mailhaut"]/div[1]').text
        print("Email di conferma spedizione arrivata correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_reso_loggato()
driver.quit()
second_driver.quit()
print("END "+nomeTest)

