#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-015_MiniCart.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_mini_cart():
    time.sleep(3)

    prodotto = str(random.randint(1, 3))

    #inserisco il n.di prodotto random nell'xpath. Viene concatenato mediante apici poichè da intero va tradotto in stringa
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li['+prodotto+']/div/div[1]/a[2]/span/span/img').click()
    time.sleep(3)
    print("Ho cliccato sul prodotto e sono nel dettaglio prodotto")

    #switch to quick view
    driver.switch_to.frame(0)
    time.sleep(2)

    #vado al dettaglio prodotto
    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    time.sleep(3)
    print("Sono nella scheda prodotto")

    nome_prodotto = driver.find_element_by_xpath('// *[ @ id = "maincontent"] / div / div / div / div[3] / div[2] / div[1] / div / h1 / span').text
    # print(nome_prodotto)
    prezzo = driver.find_element_by_css_selector("span.price").text
    # print(prezzo)
    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()

    print("Ho aggiunto un prodotto al carrello")
    time.sleep(5)

    carrello = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text

    print("Dopo aver aggiunto un prodotto al carrello controllo il mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[2]/div[2]/div/div/div/div[1]').click()
    time.sleep(5)

    prodotto_cart = driver.find_element_by_xpath('//*[@id="mini-cart"]/li/div/div/strong/a').text
    # print(prodotto_cart)

    prezzo_cart = driver.find_element_by_xpath('//*[@id="mini-cart"]/li/div/div/div[1]/div[1]/span/span/span/span').text
    # print(prezzo_cart)

    if(nome_prodotto == prodotto_cart and prezzo == prezzo_cart):
        print("Mini cart caricato correttamente")

    print("Vado ad eliminare il prodotto dal mini-cart")
    driver.find_element_by_xpath('//*[@id="mini-cart"]/li/div/div/div[2]/div[2]/a').click()
    time.sleep(3)

    print("Sto x confermare l'eliminazione del prodotto dal mini-cart")
    driver.find_element_by_xpath("//footer/button[2]/span").click()
    time.sleep(3)

    carrello_1 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text
    # print(carrello_1)

    if(float(carrello) > float(carrello_1)):
        print("Eliminazione prodotto dal mini-cart corretta")

# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_mini_cart()
driver.quit()
print("END "+nomeTest)