#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-020_CheckoutGuestSuccess.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def mailinatorAccess():
    print("\nsto per aprire Mailinator")
    second_driver.get("https://www.mailinator.com/")
    print("ho aperto Mailinator")
    try:
        element = WebDriverWait(second_driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="inboxfield"]'))
        )
        element = WebDriverWait(second_driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button'))
        )
        second_driver.find_element_by_xpath('//*[@id="inboxfield"]').click()
        casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(3)]
        casuale = ''.join(casuale_1)
        emailCasuale = "fitt_guest_"+casuale+"@mailinator.com"
        second_driver.find_element_by_xpath('//*[@id="inboxfield"]').send_keys(emailCasuale)
        second_driver.find_element_by_xpath('/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button').click()
    finally:
        print("ho generato la mail "+emailCasuale)

    config.set('SEZIONE_GUEST', 'email_guest', emailCasuale)
    with open(filediconfigurazione, 'w') as configfile:
         config.write(configfile)

    #salvo la url di riferimento nel ConfigFile
    url_guest = second_driver.current_url
    config.set('SEZIONE_GUEST', 'url_guest', url_guest)
    with open(filediconfigurazione, 'w') as configfile:
        config.write(configfile)

def test_checkout_guest_success():

    driver.switch_to.window(driver.window_handles[0])

    #prendo i dati per effettuare la registrazione dal configFile
    email = config.get('SEZIONE_GUEST', 'email_guest')
    nome =  config.get('SEZIONE_GUEST', 'nome_guest')
    cognome = config.get('SEZIONE_GUEST', 'cognome_guest')
    telefono = config.get('SEZIONE_GUEST', 'telefono_guest')
    # url = config.get('SEZIONE_GUEST', 'url_guest')
    time.sleep(2)

    prodotto = str(random.randint(1,3))

    element_to_hover_over = driver.find_element_by_css_selector(
        '#nav > li.level0.nav-3.level-top.parent > a > span:nth-child(2)')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()
    time.sleep(3)

    d = 0
    size = [0 for x in range(d)]
    i = 0

    all_option = driver.find_elements_by_tag_name('div')
    for option in all_option:
        if option.get_attribute('class') == 'swatch-option text':
            lung = option.get_attribute('option-id')
            size.append(lung)
            d = d + 0
            i = i + 1

    lunghezza = (len(size))

    a = random.randint(0 , lunghezza-1)
    b = str(size[a])

    driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-' + b +'"]').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    time.sleep(2)
    print("Ho aggiunto un prodotto al carrello")

    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div/a/img').click()

    prodotto = str(random.randint(1, 2))

    print("Sto per cliccare sul prodotto")
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li[' + prodotto + ']/div/div[1]/a[2]/span/span/img').click()
    time.sleep(3)
    print("Ho cliccato sul prodotto e sono nello Store View")

    time.sleep(5)
    driver.switch_to.frame(0)

    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    print("Sono andato alla scheda prodotto")
    time.sleep(5)

    driver.find_element_by_xpath('//*[@id="product_addtocart_form"]/div/div/div[2]').click()
    print("Ho aggiunto un prodotto al carrello dalla 'Scheda prodotto'")
    time.sleep(5)

    print("Dopo aver aggiunto dei prodotti al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[3]/div[1]/div/div/div/div[1]/a').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vai al Checkout'")
    driver.find_element_by_xpath('//*[@id="top-cart-btn-checkout"]').click()
    time.sleep(10)

    try:
        assert "CHECKOUT" in driver.find_element_by_xpath('//*[@id="checkout"]/div[1]/h1').text
        print("Sono al Checkout")
    finally:
        time.sleep(3)

    try:
        assert "INDIRIZZO DI SPEDIZIONE" in driver.find_element_by_id("shipping_step_header").text
        print("Prima colonna caricata")
    finally:
        time.sleep(3)

    try:
        assert "METODI DI SPEDIZIONE" in driver.find_element_by_id("shipping_method_step_header").text
        print("Seconda colonna caricata")
    finally:
        time.sleep(3)

    try:
        assert "RIVEDI ORDINE" in driver.find_element_by_id("review_step_header").text
        print("Terza colonna caricata")
    finally:
        time.sleep(3)

    print("Seleziono un metodo di pagamento")
    driver.find_element_by_xpath('//*[@id="checkout-payment-method-load"]/div/div/div[2]/div[1]/label/span').click()
    time.sleep(3)

    print("Clicco su 'Effettua Ordine' per controllare i messaggi di warning")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(3)

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("customer-email-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo email")

    print("Sto per eliminare un prodotto dal carrello")
    driver.find_element_by_xpath("//tr[2]/td[3]/div[2]").click()
    time.sleep(3)
    driver.find_element_by_xpath("//button[2]").click()
    time.sleep(3)
    print("Ho eliminato un prodotto dal carrello")

    print("Inserisco i dati per l'ordine")
    time.sleep(3)
    driver.find_element_by_id("customer-email").click()
    driver.find_element_by_id("customer-email").clear()
    driver.find_element_by_id("customer-email").send_keys(email)
    print("Ho inserito l'email")

    driver.find_element_by_name('firstname').click()
    driver.find_element_by_name('firstname').send_keys(nome)
    print("Ho inserito il nome")

    driver.find_element_by_name('lastname').click()
    driver.find_element_by_name('lastname').send_keys(cognome)
    print("Ho inserito il cognome")

    driver.find_element_by_name('street[0]').click()
    driver.find_element_by_name('street[0]').send_keys("Pedro Alvarez Cabrai, 16")
    print("Ho inserito l'indirizzo")

    driver.find_element_by_name('region').click()
    Select(driver.find_element_by_name("region")).select_by_visible_text("Cosenza")
    print("Ho selezionato la provincia")

    driver.find_element_by_name('city').click()
    Select(driver.find_element_by_name("city")).select_by_visible_text("Rende")
    print("Ho selezionato la città")

    driver.find_element_by_name('postcode').click()
    Select(driver.find_element_by_name("postcode")).select_by_visible_text("87036")
    print("Ho selezionato il CAP")

    driver.find_element_by_name('telephone').click()
    driver.find_element_by_name('telephone').send_keys(telefono)
    print("Ho inserito il numero di telefono")

    print("Seleziono l'iscrizione alla newsletter")
    driver.find_element_by_xpath('//*[@id="checkout"]/div[4]/div[3]/div[3]/label').click()
    time.sleep(5)

    print("Inserisco un commento")
    driver.find_element_by_id("order_comments").click()
    driver.find_element_by_id("order_comments").send_keys("Test prova")
    time.sleep(3)

    print("Sto per effettuare l'ordine")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(3)

    print("Vado a controllare che l'ordine sia stato concluso con successo")
    try:
        assert "Grazie per aver effettuato il tuo acquisto !!!" in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/h2').text
        print("Ordine effettuato con successo")
    finally:
        time.sleep(3)

    n_ordine = driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/p[1]/span').text

    # salvo il numero ordine nel ConfigFile
    config.set('SEZIONE_GUEST', 'n_ordine_guest', n_ordine)
    with open(filediconfigurazione, 'w') as configfile:
        config.write(configfile)

    second_driver.switch_to_window(second_driver.window_handles[-1])
    time.sleep(5)

    try:
        print("Sto per aprire l'email dell'Iscrizione alla newsletter")
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div.all_message-min_text.all_message-min_text-3")))
        second_driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
    finally:
        print("Ho aperto la mail dell'Iscrizione alla newsletter")

    second_driver.switch_to.frame("msg_body")
    time.sleep(3)

    try:
        assert "Sei stato iscritto alla nostra newsletter." in second_driver.find_element_by_css_selector("td.main-content").text
        print("Email 'Iscrizione alla Newsletter' ricevuta correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

mailinatorAccess()
test_checkout_guest_success()
driver.quit()
second_driver.quit()
print("END " + nomeTest)