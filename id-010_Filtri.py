#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-010_filtri.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_filtri():
    time.sleep(3)

    driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[2]/a/span").click()
    time.sleep(3)
    print("ho cliccato su 'Accessori'")

    casuale = str(random.randint(1, 2))
    driver.find_element_by_xpath('//*[@id="narrow-by-list"]/div/div[2]/ol/li['+ casuale +']/a/span[1]').click()
    print("Ho selezionato un filtro")
    time.sleep(3)

    print("Sto per eliminare il filtro")
    driver.find_element_by_xpath('//*[@id="layered-filter-block"]/div[2]/div[2]/a/span').click()
    print("Ho de-selezionato il filtro")


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_filtri()
driver.quit()
print("END "+nomeTest)