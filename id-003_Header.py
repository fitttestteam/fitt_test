#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import re
import configparser

nomeTest = 'id-003_Header.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_header():
    time.sleep(3)

    driver.find_element_by_xpath('//div/div/div/div[2]/nav/div/ul/li/a/span').click()
    time.sleep(5)
    print("ho cliccato su 'Domande e Risposte'")
    print("Verifico il corretto caricamento della pagina 'FAQ'")

    try:
        assert "FAQ" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento pagina 'FAQ' corretta")
    finally:
        time.sleep(2)
        print("Vado a verificare il corretto caricamento della pagina 'Accessori'")

    driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[2]/a/span").click()
    time.sleep(3)
    print("ho cliccato su 'Accessori'")

    try:
        assert "Accessori" in driver.find_element_by_xpath("//h1[@id='page-title-heading']/span").text
        print("Caricamento pagina 'Accessori' corretta")
    finally:
        time.sleep(2)
        print("Vado a verificare il corretto caricamento della pagina 'News'")

    driver.find_element_by_xpath('//*[@id="nav"]/li[4]/a').click()
    time.sleep(3)
    print("Ho cliccato su 'News'")

    try:
        assert  "Ultime Notizie Postate" in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[1]/h1').text
        print("caricamento pagina 'News' corretta")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_header()
driver.quit()
print("END "+nomeTest)