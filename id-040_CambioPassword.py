#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-040_CambioPassword.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_cambio_password():

    driver.switch_to.window(driver.window_handles[0])

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')
    url_reg = config.get('SEZIONE_REGISTRAZIONE', 'urlgen')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[3]/div[2]/ul/li[1]/span').click()
    driver.find_element_by_link_text("Il mio Account").click()

    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    print("Clicco su cambia password")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div[2]/div[1]/div[2]/a[2]').click()
    time.sleep(3)

    print("Inserisco la password attuale")
    driver.find_element_by_id("current-password").click()
    driver.find_element_by_id("current-password").send_keys(password)
    time.sleep(3)

    print("Inserisco la nuova password")
    driver.find_element_by_id("password").click()
    driver.find_element_by_id("password").send_keys(password)
    time.sleep(3)

    print("Confermo la nuova password")
    driver.find_element_by_id("password-confirmation").click()
    driver.find_element_by_id("password-confirmation").send_keys(password)
    time.sleep(3)

    print("Clicco su salva")
    driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
    time.sleep(3)

    try:
        assert "Hai salvato le informazioni sull'account." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
        print("Password salvata correttamente")
    finally:
        time.sleep(3)

    second_driver.switch_to_window(second_driver.window_handles[-1])
    time.sleep(5)
    second_driver.get(url_reg)

    email = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    print("Vado a controllare se è arrivata l'email di conferma spedizione")
    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys(email)
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="f"]/table/tbody/tr[1]/td[3]').click()

    second_driver.switch_to.frame("ifinbox")
    time.sleep(3)

    second_driver.find_element_by_xpath('//*[@id="m1"]/div/a/span[2]').click()
    print("Ho aperto l'email di modifica password")

    second_driver.switch_to.default_content()

    second_driver.switch_to.frame("ifmail")
    time.sleep(3)

    try:
        assert u"È stata modificata la password di YOYO by FITT IT" in second_driver.find_element_by_xpath('//*[@id="mailhaut"]/div[1]').text
        print("Email di cambio password arrivata correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_cambio_password()
driver.quit()
second_driver.quit()
print("END " + nomeTest)
