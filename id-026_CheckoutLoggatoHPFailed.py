#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-026_CheckoutLoggatoHPFailed.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_checkout_loggato_hp_failed():

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    prodotto = str(random.randint(1,3))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()
    time.sleep(3)

    d = 0
    size = [0 for x in range(d)]
    i = 0

    all_option = driver.find_elements_by_tag_name('div')
    for option in all_option:
        if option.get_attribute('class') == 'swatch-option text':
            lung = option.get_attribute('option-id')
            size.append(lung)
            d = d + 0
            i = i + 1

    lunghezza = (len(size))

    a = random.randint(0 , lunghezza-1)
    b = str(size[a])

    driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-' + b +'"]').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    time.sleep(2)
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(5)

    print("Dopo aver aggiunto un prodotto al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[2]/div[2]/div/div/div/div[1]').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vai al Checkout'")
    driver.find_element_by_xpath('//*[@id="top-cart-btn-checkout"]').click()
    time.sleep(5)

    try:
        assert "CHECKOUT" in driver.find_element_by_xpath('//*[@id="checkout"]/div[1]/h1').text
        print("Sono al Checkout")
    finally:
        time.sleep(3)

    print("Seleziono un metodo di pagamento")
    driver.find_element_by_xpath("//div[@id='checkout-payment-method-load']/div/div/div[3]/div/label").click()
    print("Ho selezionato 'Banca Sella'")
    time.sleep(5)

    print("Inserisco il numero della carta")
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").clear()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").send_keys("4775718800003024")

    print("Inserisco il mese di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]')).select_by_visible_text("05 - maggio")

    print("Inserisco l'anno di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]')).select_by_visible_text("2027")

    print("Inserisco il codice CVV")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').click()
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').send_keys("646")

    print("Inserisco il nome cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").send_keys("Mario Rossi")

    print("Inserisco l'email cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").send_keys("mariorossi@future.com")
    time.sleep(5)

    print("Inserisco un commento")
    driver.find_element_by_id("order_comments").click()
    driver.find_element_by_id("order_comments").send_keys("Test prova")
    time.sleep(3)

    print("Sto per effettuare l'ordine")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(5)

    try:
        assert "Payment transaction not authorized: Autorizzazione negata." in driver.find_element_by_xpath(
            "//main[@id='maincontent']/div/div/div/div[2]/div[3]/div/div").text
        print("Pagamento negato")
    finally:
        time.sleep(3)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_checkout_loggato_hp_failed()
driver.quit()
print("END " + nomeTest)

