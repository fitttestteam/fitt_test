#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import re
import configparser

nomeTest = 'id-005_Footer.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_footer():
    time.sleep(3)

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(5)

    try:
        assert "Newsletter" in driver.find_element_by_css_selector("div.footer-block-title > h5").text
        print("verifica caricamento modulo iscrizione 'Newsletter' OK")
    finally:
        time.sleep(3)

    try:
        assert "Chi siamo" in driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[1]/a').text
        print("verifica caricamento 'Chi siamo' OK")
    finally:
        time.sleep(2)

    try:
        assert "Metodi di pagamento" in driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[2]/a').text
        print("verifica caricamento 'Metodi di Pagamento' OK")
    finally:
        time.sleep(2)

    try:
        assert "Privacy Policy" in driver.find_element_by_xpath(
            '/html/body/div[1]/footer/div/div/div/div[1]/div/div[3]/div[2]/ul/li[1]/a').text
        print("verifica caricamento 'Privacy Policy' OK")
    finally:
        time.sleep(2)

    try:
        assert "Social" in driver.find_element_by_xpath(
            '/html/body/div[1]/footer/div/div/div/div[1]/div/div[4]/div[2]/div[2]/h5').text
        print("verifica caricamento modulo 'Social' OK")
    finally:
        time.sleep(2)



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_footer()
driver.quit()
print("END "+nomeTest)