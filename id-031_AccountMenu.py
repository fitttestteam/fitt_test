#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-031_AccountMenu.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_account_menu():

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath("//span/span").click()
    driver.find_element_by_link_text("Il mio Account").click()
    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("I Miei ordini").click()
    print("Ho cliccato su 'I Miei ordini' dal menù sulla sinistra")

    try:
        assert "I Miei ordini" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Rubrica").click()
    print("Ho cliccato su 'Rubrica' dal menù sulla sinistra")

    try:
        assert "Rubrica" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Informazioni Account").click()
    print("Ho cliccato su 'Informazioni Account' dal menù sulla sinistra")

    try:
        assert "Modificare le informazioni Account" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Iscrizioni alla newsletter").click()
    print("Ho cliccato su 'Iscrizioni alla newsletter' dal menù sulla sinistra")

    try:
        assert "Iscrizione alla newsletter" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Help Desk").click()
    print("Ho cliccato su 'Help Desk' dal menù sulla sinistra")

    try:
        assert "Help Desk" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Resi").click()
    print("Ho cliccato su 'Resi' dal menù sulla sinistra")

    try:
        assert "Resi" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Pagina caricata correttamente")
    finally:
        time.sleep(2)




# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_account_menu()
driver.quit()
print("END " + nomeTest)