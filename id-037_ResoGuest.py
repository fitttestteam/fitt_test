#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-037_ResoGuest.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_completa_ordine():

    url = config.get('SEZIONE_BE', 'url_1')
    n_ordine = config.get('SEZIONE_GUEST', 'n_ordine_guest')

    print("sto per aprire il BE di Fitt")
    second_driver.get(url)
    print("ho aperto il BE di Fitt")

    print("Sto per inserire le credenziali")
    second_driver.find_element_by_xpath('//*[@id="username"]').click()
    second_driver.find_element_by_xpath('//*[@id="username"]').send_keys("pasquale.piane")
    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys("Fitt2018")
    time.sleep(3)
    second_driver.find_element_by_xpath('//*[@id="login-form"]/fieldset/div[3]/div[1]/button/span').click()
    print("Ho effettuato la login in BE")

    print("Sto cliccando su Sales")
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "SALES"))
        )
        second_driver.find_element_by_link_text("SALES").click()
    finally:
        print("Ho cliccato su Sales")

    print("Sto cliccando su Orders")
    time.sleep(3)
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Orders"))
        )
        second_driver.find_element_by_link_text("Orders").click()
    finally:
        print("Ho cliccato su Orders")

    time.sleep(10)
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[3]/div/button").click()
    time.sleep(5)

    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").click()
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").clear()
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/fieldset/div[2]/div/input").send_keys(n_ordine)
    second_driver.find_element_by_xpath("//div[@id='container']/div/div[2]/div/div[5]/div/div/button[2]/span").click()
    time.sleep(5)

    second_driver.find_element_by_xpath("//div[@id='container']/div/div[4]/table/tbody/tr/td[8]").click()
    print("Ho cliccato sull'ordine effettuato in precedenza")
    time.sleep(5)

    a = second_driver.find_element_by_id("order_status").text
    print(a)

    print("Sto per cliccare su 'Invoice'")
    second_driver.find_element_by_xpath('//*[@id="order_invoice"]/span').click()
    time.sleep(3)

    print("Seleziono l'invio della mail per la creazione della fattura")
    second_driver.find_element_by_id('send_email').click()
    time.sleep(3)

    print("Sto per cliccare su 'Submit Invoice'")
    second_driver.find_element_by_xpath("//div[4]/button/span").click()
    time.sleep(3)

    try:
        assert "The invoice has been created." in second_driver.find_element_by_xpath("//div[@id='messages']/div/div/div").text
        print("Fattura creata correttamente")
    finally:
        time.sleep(3)
        print("Sto per cliccare su 'Ship'")

    second_driver.find_element_by_xpath('//*[@id="order_ship"]/span').click()
    time.sleep(2)
    print("Ho cliccato su 'Ship")

    print("Seleziono l'invio email per la conferma di spedizione")
    second_driver.find_element_by_id('send_email').click()
    time.sleep(3)

    print("Sto per cliccare su 'Submit Shipment")
    second_driver.find_element_by_xpath("//div[3]/button/span").click()
    time.sleep(3)

    try:
        assert "The shipment has been created." in second_driver.find_element_by_xpath(
            "//div[@id='messages']/div/div/div").text
        print("Spedizione effettuta con successo")
    finally:
        time.sleep(5)


def test_reso_guest():

    driver.switch_to.window(driver.window_handles[0])

    email = config.get('SEZIONE_GUEST', 'email_guest')
    n_ordine =  config.get('SEZIONE_GUEST', 'n_ordine_guest')
    url = config.get('SEZIONE_GUEST', 'url_guest')

    driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[5]/a').click()
    time.sleep(3)

    print("Inserisco il numero d'ordine che voglio fare il reso")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div[2]/form/fieldset/div[2]/div/input').click()
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div[2]/form/fieldset/div[2]/div/input').send_keys(n_ordine)
    time.sleep(2)

    print("Inserisco l'email con cui ho fatto l'ordine")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div[2]/form/fieldset/div[3]/div/input').click()
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div[2]/form/fieldset/div[3]/div/input').send_keys(email)
    time.sleep(2)

    print("Sto per cliccare su 'Successivo'")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div[2]/form/fieldset/div[4]/div/button').click()
    time.sleep(5)

    print("Seleziono l'oggetto da rendere")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div/form/fieldset[2]/div[2]/div/div[1]/div[1]/input').click()
    time.sleep(3)

    print("Seleziono il tipo di Risoluzione")
    Select(driver.find_element_by_name("custom_fields[1]")).select_by_visible_text("Rimborso")
    time.sleep(3)

    print("Seleziono le condizioni del prodotto")
    Select(driver.find_element_by_xpath("//div[2]/div/select")).select_by_visible_text("Non aperto")
    time.sleep(3)

    print("Seleziono le 'Motivazioni del reso'")
    Select(driver.find_element_by_xpath("//div[3]/div[2]/div/select")).select_by_visible_text("Ho cambiato idea")
    time.sleep(3)

    print("Sto per cliccare su 'Invia Richiesta'")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div/form/div/button').click()
    time.sleep(5)

    print("Salvo il numero di reso")
    temp = driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[1]/h1/span').text
    # print(temp)

    reso = temp[28:]
    print(reso)

    driver.get(url)

    try:
        print("Sto per controllare se l'email di reso è arrivata")
        element = WebDriverWait(driver, 100).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div.all_message-min_text.all_message-min_text-3")))
        driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
    finally:
        print("Ho aperto la mail per la conferma del Reso")
        time.sleep(5)

    a = driver.find_element_by_xpath('//*[@id="msgpane"]/div[1]/div[1]').text
    # print(a)

    reso_email = a[5:-34]
    print(reso_email)

    if(reso == reso_email):
        print("Reso creato correttamente")

    second_driver.switch_to_window(second_driver.window_handles[-1])
    second_driver.refresh()

    print("Sto cliccando su Sales")
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "SALES"))
        )
        second_driver.find_element_by_link_text("SALES").click()
    finally:
        print("Ho cliccato su Sales")

    print("Sto cliccando su 'Manage RMA'")
    time.sleep(3)
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Manage RMA"))
        )
        second_driver.find_element_by_link_text("Manage RMA").click()
    finally:
        print("Ho cliccato su 'Manage RMA'")

    print("Filtro i resi con il numero di reso salvato in frontend")
    time.sleep(5)
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[2]/div[1]/div[2]/div/button').click()
    time.sleep(5)

    second_driver.find_element_by_name('increment_id').click()
    second_driver.find_element_by_name('increment_id').clear()
    second_driver.find_element_by_name('increment_id').send_keys(reso_email)
    time.sleep(3)
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[2]/div[1]/div[4]/div/div/button[2]').click()
    time.sleep(3)

    print("Entro nella pagina del reso per controllare che sia corretto")
    second_driver.find_element_by_xpath('//*[@id="container"]/div/div[3]/table/tbody/tr/td[2]/div/a').click()
    time.sleep(5)

    print("Salvo il numero del reso di Backend")
    a = second_driver.find_element_by_xpath("//body[@id='html-body']/div[2]/header/div/div/h1").text
    reso_be = a[16:]
    print(reso_be)

    print("Confronto i due codici per verificae la corretta funzione di reso")
    if (reso == reso_be):
        print("Reso inserito correttamente")

    # url_spedito = config.get('SEZIONE_GUEST', 'url_guest')
    # second_driver.get(url_spedito)
    driver.switch_to.window(driver.window_handles[0])
    print("Vado a controllare se è arrivata l'email di conferma spedizione")
    driver.back()
    time.sleep(5)

    driver.find_element_by_xpath("//li[2]/div/div[4]").click()
    try:
        assert u"Il tuo ordine su YOYO by FITT IT è stato spedito" in driver.find_element_by_css_selector("#msgpane > div > div.ng-binding").text
        print("Email conferma spedizione arrivata correttamente")
    finally:
        time.sleep(3)



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_completa_ordine()
test_reso_guest()
driver.quit()
second_driver.quit()
print("END "+nomeTest)