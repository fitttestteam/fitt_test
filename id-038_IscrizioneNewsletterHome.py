#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-038_IscrizioneNewsletterHome.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)


def mailinatorAccess():

    # leggo il file di properties che contiene la password iniziale
    config = configparser.RawConfigParser()
    config.read(filediconfigurazione)

    print("\nsto per aprire Mailinator")
    second_driver.get("https://www.mailinator.com/")
    print("ho aperto Mailinator")

    try:
        element = WebDriverWait(second_driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="inboxfield"]'))
        )
        element = WebDriverWait(second_driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button'))
        )
        second_driver.find_element_by_xpath('//*[@id="inboxfield"]').click()
        casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(3)]
        casuale = ''.join(casuale_1)
        emailCasuale = "newsletter_fitt_"+casuale+"@mailinator.com"
        second_driver.find_element_by_xpath('//*[@id="inboxfield"]').send_keys(emailCasuale)
        second_driver.find_element_by_xpath('/html/body/section[1]/div/div[3]/div[2]/div[2]/div[1]/span/button').click()
    finally:
        print("ho generato la mail "+emailCasuale)

    config.set('NEWSLETTER_GUEST', 'email_nl', emailCasuale)
    with open(filediconfigurazione, 'w') as configfile:
         config.write(configfile)

    #salvo la url di riferimento nel ConfigFile
    url = second_driver.current_url
    config.set('NEWSLETTER_GUEST', 'url_nl', url)
    with open(filediconfigurazione, 'w') as configfile:
        config.write(configfile)

    time.sleep(5)



def test_iscrizione_newsletter_home():

    # leggo il file di properties che contiene la password iniziale
    config = configparser.RawConfigParser()
    config.read(filediconfigurazione)

    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(3)

    print("Clicco su 'Iscriviti' per verificare il messaggio di Warning")
    driver.find_element_by_xpath('//*[@id="newsletter-footer-validate-detail"]/div[2]/button').click()
    time.sleep(3)

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("newsletter-footer-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo email")

    print("Insersco una email non valida per controllare se esce il messaggio di Errore")
    driver.find_element_by_id('newsletter-footer').click()
    driver.find_element_by_id('newsletter-footer').send_keys("aaaa")
    time.sleep(3)

    print("Dopo aver inserito una email non valida clicco su 'Iscriviti' per controllare se comapre il messaggio d'Errore")
    driver.find_element_by_xpath('//*[@id="newsletter-footer-validate-detail"]/div[2]/button').click()
    time.sleep(3)

    try:
        assert u"Inserisci un indirizzo email valido." in driver.find_element_by_id("newsletter-footer-error").text
    finally:
        time.sleep(3)
        print("Messaggio d'Errore uscito correttamente nel campo email")

    email = config.get('NEWSLETTER', 'email_nl')

    print("Inserisco l'email per l'iscrizione alla newsletter")
    driver.find_element_by_id('newsletter-footer').click()
    driver.find_element_by_id('newsletter-footer').clear()
    driver.find_element_by_id('newsletter-footer').send_keys(email)
    time.sleep(3)

    print("Clicco su 'Iscriviti'")
    driver.find_element_by_xpath('//*[@id="newsletter-footer-validate-detail"]/div[2]/button').click()
    time.sleep(3)

    try:
        assert "Grazie per la tua iscrizione." in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[1]/div[2]/div/div/div').text
        print("Iscrizione avvenuta correttamente")
    finally:
        time.sleep(3)

    print("Vado a controllare se ho ricevuto l'email di iscrizione")
    second_driver.switch_to_window(second_driver.window_handles[-1])
    second_driver.refresh()
    time.sleep(5)

    print("Sto aprendo l'email ricevuta")
    second_driver.find_element_by_xpath("//li/div/div[4]").click()
    time.sleep(5)
    try:
        assert "Sottoscrizione alla Newsletter avvenuta" in second_driver.find_element_by_xpath('//*[@id="msgpane"]/div[1]/div[1]').text
        print("Email ricevuta correttamente")
    finally:
        time.sleep(3)

    print("Vado sul BE per controllare l'iscrizione")

    url = config.get('SEZIONE_BE', 'url_1')

    print("Sto per aprire il BE di Fitt")
    second_driver.get(url)
    print("Ho aperto il BE di Fitt")

    print("Sto per inserire le credenziali")
    second_driver.find_element_by_xpath('//*[@id="username"]').click()
    second_driver.find_element_by_xpath('//*[@id="username"]').send_keys("pasquale.piane")
    second_driver.find_element_by_xpath('//*[@id="login"]').click()
    second_driver.find_element_by_xpath('//*[@id="login"]').send_keys("Fitt2018")
    time.sleep(3)
    second_driver.find_element_by_xpath('//*[@id="login-form"]/fieldset/div[3]/div[1]/button/span').click()
    print("Ho effettuato la login in BE")

    print("Sto cliccando su Marketing")
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "MARKETING"))
        )
        second_driver.find_element_by_link_text("MARKETING").click()
    finally:
        print("Ho cliccato su Marketing")

    print("Sto cliccando su 'Newsletter Subscribers'")
    time.sleep(3)
    try:
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Newsletter Subscribers"))
        )
        second_driver.find_element_by_link_text("Newsletter Subscribers").click()
    finally:
        print("Ho cliccato su 'Newsletter Subscribers'")
        time.sleep(5)

    print("Filtro i risultati con l'email appena utilizzata")
    second_driver.find_element_by_id('subscriberGrid_filter_email').click()
    second_driver.find_element_by_id('subscriberGrid_filter_email').send_keys(email)
    time.sleep(5)

    second_driver.find_element_by_xpath("//div/div/div/div/div/div/div/button/span").click()
    print("Ho cliccato su 'Search'")
    time.sleep(5)

    try:
        assert "Subscribed" in second_driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='----'])[2]/following::td[1]").text
        print("Iscrizione alla Newsletter presente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

mailinatorAccess()
test_iscrizione_newsletter_home()
driver.quit()
second_driver.quit()
print("END "+nomeTest)