#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import re
import configparser

nomeTest = 'id-001_HomePage.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_home_page():
    time.sleep(3)
    try:
        assert 'DOMANDE E RISPOSTE' in driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li/a/span").text
        print("'DOMANDE E RISPOSTE' presente nell'Header")
    finally:
        time.sleep(3)

    try:
        assert 'ACCESSORI' in driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[2]/a/span").text
        print("'ACCESSORI' presente nell'Header")
    finally:
        time.sleep(3)

    try:
        assert 'PRODOTTI' in driver.find_element_by_xpath('//*[@id="nav"]/li[3]/a/span[2]').text
        print("'PRODOTTI' presente nell'Header")
    finally:
        time.sleep(3)

    try:
        assert 'NEWS' in driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[4]/a/span").text
        print("'NEWS' presente nell'Header")
    finally:
        time.sleep(3)

    try:
        assert "Prodotto dell'Anno 2018" in driver.find_element_by_css_selector('#maincontent > div > div > div > div.homepage_blocco_5 > div.text-right-box > div > h3').text
        print("verifica caricamento contenuti HomePage OK")
    finally:
        time.sleep(3)

    try:
        assert u"Chi è FITT" in driver.find_element_by_css_selector('#maincontent > div > div > div > div.homepage_blocco_9 > div > div.homepage_blocco_9_left > h2').text
        print("verifica caricamento contenuti finali HomePage OK")
    finally:
        time.sleep(3)

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(5)

    try:
        assert "Newsletter" in driver.find_element_by_css_selector("div.footer-block-title > h5").text
        print("verifica caricamento Footer OK")
    finally:
        time.sleep(3)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_home_page()
driver.quit()
print("END "+nomeTest)

# #invio mail di report
# from ReportToEmail import ReportToEmail
# report = ReportToEmail()
#
# data = time.strftime("%d/%m/%Y - ore %H:%M:%S")
# oggetto = nomeTest+' - OK - eseguito il '+data
# messaggio = 'END '+nomeTest
# report.sendemail(subject = oggetto, message = messaggio)
#
# unittest.main()