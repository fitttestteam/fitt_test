#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-039_IscrizioneNewsletterReg.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_iscrizione_newsletter_reg():

    # leggo il file di properties che contiene la password iniziale
    config = configparser.RawConfigParser()
    config.read(filediconfigurazione)

    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(3)

    email = config.get('NEWSLETTER_REG', 'emailReg_nl')
    url = config.get('NEWSLETTER_REG', 'urlReg_nl')
    password = config.get('NEWSLETTER_REG', 'pw')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[3]/div[2]/ul/li[1]/span/span').click()
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[3]/div[2]/ul/li[1]/div/ul/li[1]/a').click()
    print("Sono sulla mia Bacheca")

    situation = driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div[2]/div[2]/div[1]').text
    print(situation)

    if(situation == 'Sei iscritto a "Iscrizioni Generali".'):

        try:
            assert 'Sei iscritto a "Iscrizioni Generali".' in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div[2]/div[2]/div[1]').text
            print("Iscrizione alla newsletter presente")
        finally:
            time.sleep(3)

        print("Vado ad effettuare la cancellazione dalla newsletter")
        driver.find_element_by_xpath('//*[@id="account-nav"]/ul/li[7]/a').click()
        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="subscription"]').click()
        time.sleep(3)
        print("Ho deselezionato l'iscrizione")

        driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
        print("Ho cliccato su salva")

        try:
            assert u"L'iscrizione è stata rimossa." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
            print("Iscrizione alla newsletter eliminata")
        finally:
            time.sleep(2)

        print("Vado a controllare se ho ricevuto l'email di conferma")
        second_driver.switch_to_window(second_driver.window_handles[-1])
        second_driver.get(url)
        time.sleep(3)

        print("Apro l'email ricevuta")
        second_driver.find_element_by_xpath('//li/div/div[4]').click()
        time.sleep(3)

        try:
            assert "Cancellazione dalla Newsletter avvenuta" in second_driver.find_element_by_xpath('//*[@id="msgpane"]/div[1]/div[1]').text
            print("Email ricevuta correttamente")
        finally:
            time.sleep(3)

    else:
        print("Vado su iscrizione alla newsletter")
        driver.find_element_by_xpath('//*[@id="account-nav"]/ul/li[7]/a').click()
        time.sleep(3)

        print("Selezionato l'iscrizione")
        driver.find_element_by_xpath('//*[@id="subscription"]').click()
        time.sleep(3)

        print("Clicco su salva")
        driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
        time.sleep(3)

        try:
            assert "Abbiamo salvato l'iscrizione." in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[2]/div[2]/div/div/div').text
            print("Iscrizione alla newsletter effettuata")
        finally:
            time.sleep(2)

        print("Controllo lo stato dell'iscrizione")

        try:
            assert 'Sei iscritto a "Iscrizioni Generali".' in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div[2]/div[4]/div[2]/div[2]/div[1]').text
            print("Iscrizione alla newsletter presente")
        finally:
            time.sleep(3)

        print("Vado ad effettuare la cancellazione dalla newsletter")
        driver.find_element_by_xpath('//*[@id="account-nav"]/ul/li[7]/a').click()
        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="subscription"]').click()
        time.sleep(3)
        print("Ho deselezionato l'iscrizione")

        driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
        print("Ho cliccato su salva")

        a = driver.find_element_by_css_selector("div.message-success.success.message > div").text

        try:
            assert u"L'iscrizione è stata rimossa." in driver.find_element_by_css_selector("div.message-success.success.message > div").text
            print("Iscrizione alla newsletter eliminata")
        finally:
            time.sleep(2)

        print("Vado a controllare se ho ricevuto l'email di conferma")
        second_driver.switch_to_window(second_driver.window_handles[-1])
        second_driver.get(url)
        time.sleep(3)

        print("Apro l'email ricevuta")
        second_driver.find_element_by_xpath('//li/div/div[4]').click()
        time.sleep(3)

        try:
            assert "Cancellazione dalla Newsletter avvenuta" in second_driver.find_element_by_xpath('//*[@id="msgpane"]/div[1]/div[1]').text
            print("Email ricevuta correttamente")
        finally:
            time.sleep(3)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_iscrizione_newsletter_reg()
driver.quit()
second_driver.quit()
print("END "+nomeTest)