#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time, random
import re
import configparser

nomeTest = 'id-004_Prodotti.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_prodotti():
    time.sleep(3)

    carrello = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text
    # print(carrello)

    prodotto = str(random.randint(1, 3))
    # prodotto = str(1)
    # print(prodotto)

    nome_prodotto = driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li['+ prodotto +']/div/div[2]/strong/a').text
    print("Salvo il nome del prodotto selezionato")

    print("Sto per cliccare sul prodotto")
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li['+ prodotto +']/div/div[1]/a[2]/span/span/img').click()
    print("Ho cliccato sul prodotto e sono nel dettaglio prodotto")

    time.sleep(5)
    driver.switch_to.frame(0)

    prezzo = driver.find_element_by_css_selector("span.price").text
    # print(prezzo)

    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    print("Sono andato alla scheda prodotto")
    time.sleep(5)

    nome_dettaglio_prodotto = driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div[3]/div[2]/div/div/h1/span").text
    # print(nome_dettaglio_prodotto)

    prezzo_1 = driver.find_element_by_css_selector("span.price").text
    # print(prezzo_1)

    if(nome_prodotto == nome_dettaglio_prodotto and prezzo == prezzo_1):
        print("Caricato prodotto corretto")

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(3)

    carrello_1 = driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text
    # print(carrello_1)

    if(carrello < carrello_1):
        print("Prodotto aggiunto al carrello correttamente")




# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_prodotti()
driver.quit()
print("END "+nomeTest)