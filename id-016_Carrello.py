#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-016_Carrello.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_carrello():

    prodotto = str(random.randint(1, 1))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()

    try :
        driver.implicitly_wait(10)
        element = WebDriverWait(driver,20).until(
            EC.presence_of_all_elements_located((By.ID,'option-label-lunghezza-152' ))
        )
        driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-4"]').click()
        print("Ho selezionato una lunghezza")
        prezzo = driver.find_element_by_css_selector("span.price").text
        driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    finally:
        time.sleep(2)
        print("Ho aggiunto un prodotto al carrello")

    driver.find_element_by_xpath('/html/body/div[1]/div[1]/header/div/div[1]/div/a/img').click()

    prodotto = str(random.randint(1, 3))

    print("Sto per cliccare sul prodotto")
    driver.find_element_by_xpath('//*[@id="widget-products-6696"]/div/div/ol/li[' + prodotto + ']/div/div[1]/a[2]/span/span/img').click()
    time.sleep(3)
    print("Ho cliccato sul prodotto e sono nel dettaglio prodotto")

    time.sleep(5)
    driver.switch_to.frame(0)

    driver.find_element_by_xpath('//*[@id="product-gotoproduct-button"]/span').click()
    print("Sono andato alla scheda prodotto")
    time.sleep(5)

    driver.find_element_by_xpath('//*[@id="product_addtocart_form"]/div/div/div[2]').click()
    print("Ho aggiunto un prodotto al carrello dalla 'Scheda prodotto'")

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="product-addtocart-button-sticky"]').click()

    print("Ho cliccato su 'Aggiungi al carrello da 'buy button'")
    time.sleep(5)
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(3)

    print("Dopo aver aggiunto dei prodotti al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[2]/div[2]/div/div/div/div[1]').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vedi Carrello'")
    driver.find_element_by_xpath('//*[@id="minicart-content-wrapper"]/div[2]/div[4]/div[1]/a').click()
    print("Sono al carrello")
    time.sleep(3)

    print("Sto per aumentare la quantita di un prodotto")
    driver.find_element_by_xpath("//div[2]/i").click()
    print("Quantità aumentata del primo prodotto")
    time.sleep(3)
    driver.find_element_by_xpath("//tbody[2]/tr/td[3]/div/div/div/i").click()
    print("Quantità diminuita del secondo prodotto prodotto")
    time.sleep(3)
    driver.find_element_by_xpath("//button[2]").click()
    print("Ho aggiornato il carrello")

    print("Sto per eliminare un prodotto dal carrello")
    driver.find_element_by_xpath("//tbody[2]/tr/td[5]/a/i").click()
    time.sleep(3)
    print("Prodotto eliminato")

    try:
        assert "APPLICA IL CODICE SCONTO" in driver.find_element_by_xpath('//*[@id="block-discount-heading"]').text
        print("Box codice sconto presente")
    finally:
        time.sleep(2)

    print("Sto per svuotare il carrello")
    driver.find_element_by_xpath("//button[@id='empty_cart_button']/span").click()
    time.sleep(3)
    print("Carrello svuotato")

    try:
        assert "Non ci sono articoli nel tuo carrello." in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div[4]/p").text
        print("Svuotamento carrello effettuato correttamente")
    finally:
        time.sleep(2)

# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_carrello()
driver.quit()
print("END "+nomeTest)