#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-009_OrdinamentoProdotti.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_ordinamento_prodotti():
    time.sleep(3)

    driver.find_element_by_xpath("//div/div/div/div[2]/nav/div/ul/li[2]/a/span").click()
    time.sleep(3)
    print("ho cliccato su 'Accessori'")

    driver.find_element_by_xpath('//*[@id="sorter"]').click()
    Select(driver.find_element_by_xpath('//*[@id="sorter"]')).select_by_visible_text("Prezzo")
    print("Ho selezionato il tipo di ordinamento")

    prezzoP = [0 for x in range(10)]
    i = 0

    all_option = driver.find_elements_by_tag_name('span')

    for option in all_option:
        if option.get_attribute('class') == 'price-wrapper ':
            prezzo = option.text
            prezzo_1 = prezzo[:-2]
            prezzo_2 = prezzo_1.replace(',','.')
            prezzoP[i] = float(prezzo_2)
            print(prezzoP[i])
            i = i + 1

    a = 0
    for x in range (i):
        if prezzoP[a] <= prezzoP[a+1]:
            a = a + 1

    print("ordinamento crescente corretto")

    print("vado a cambiare il tipo di ordinamento")
    driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/a/span').click()
    time.sleep(5)
    print("ordinamento decrescente selezionato")

    prezzoP_1 = [0 for x in range(10)]
    j = 0

    all_option = driver.find_elements_by_tag_name('span')

    for option_1 in all_option:
        if option_1.get_attribute('class') == 'price-wrapper ':
            prezzo = option_1.text
            prezzo_3 = prezzo[:-2]
            prezzo_4 = prezzo_3.replace(',', '.')
            prezzoP_1[j] = float(prezzo_4)
            print(prezzoP_1[j])
            j = j + 1

    b = 0
    for x in range(i):
        if prezzoP_1[b] >= prezzoP_1[b + 1]:
            b = b + 1

    print("ordinamento decrescente corretto")

# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_ordinamento_prodotti()
driver.quit()
print("END "+nomeTest)