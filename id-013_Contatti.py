#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-013_Contatti.py'
print('START '+nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)
second_driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()
# second_driver = webdriver.Chrome()
# second_driver.maximize_window()

driver.delete_all_cookies()
second_driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)
second_driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_contatti():

    driver.switch_to.window(driver.window_handles[0])
    time.sleep(3)

    driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[3]/div[2]/ul/li[4]/a').click()
    print("Ho cliccato su 'Contatti")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="contact-form"]/div/div/button/span').click()
    print("Ho cliccato su invia per controllare se escono i messaggi di warning")
    time.sleep(2)

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("name-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo 'Nome")

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("city-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo 'Città")

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("email-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo 'E-mail")

    try:
        assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("comment-error").text
    finally:
        time.sleep(3)
        print("Messaggio di warning uscito correttamente nel campo 'Come possiamo esserti utile?")

    print("Compilo i campi per inviare una richiesta")
    driver.find_element_by_id("name").click()
    driver.find_element_by_id("name").send_keys("Test")
    time.sleep(3)

    driver.find_element_by_id("city").click()
    driver.find_element_by_id("city").send_keys("Rende")
    time.sleep(3)

    email = config.get('EMAIL_RICHIESTA', 'email')

    driver.find_element_by_id("email").click()
    driver.find_element_by_id("email").send_keys(email)
    time.sleep(3)

    driver.find_element_by_id("comment").click()
    driver.find_element_by_id("comment").send_keys("Test Prova")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="form-contact-privacy-1"]').click()
    time.sleep(5)
    print("Ho cliccato sul trattamento dei dati per la richiesta di informazioni")

    driver.find_element_by_xpath('//*[@id="form-contact-privacy-2"]').click()
    time.sleep(5)
    print("Ho cliccato sul trattamento dei dati per il marketing")

    driver.find_element_by_xpath('//*[@id="contact-form"]/div/div/button/span').click()
    print("Ho cliccato su invia")
    time.sleep(2)

    try:
       assert u"Il Ticket è stato creato con successo" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div[2]/div[2]/div/div/div").text
       print("Verifico che la richiesta sia stata inviata")
    finally:
        time.sleep(3)
        print("Vado ad aprire l'email per controllare aver ricevuto la conferma")

    url = config.get('EMAIL_RICHIESTA', 'url')
    second_driver.get(url)

    second_driver.switch_to_window(second_driver.window_handles[-1])

    try:
        print("Sto per aprire l'email ricevuta")
        element = WebDriverWait(second_driver, 100).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div.all_message-min_text.all_message-min_text-3")))
        second_driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
    finally:
        print("Ho aperto la mail ricevuta")

    print("Sto per controllare se la richiesta è stata inviata correttamente")
    second_driver.switch_to.frame("msg_body")
    time.sleep(5)

    try:
        assert "Hello, Test." in second_driver.find_element_by_xpath("//strong").text
        print("Richiesta contatto inserita correttamente")
    finally:
        time.sleep(2)



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_contatti()
driver.quit()
second_driver.quit()
print("END "+nomeTest)