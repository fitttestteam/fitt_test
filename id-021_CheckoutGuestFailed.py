#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-021_CheckoutGuestFailed.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_checkout_guest_failed():

    #prendo i dati per effettuare la registrazione dal configFile
    email = config.get('SEZIONE_GUEST', 'email_guest')
    nome =  config.get('SEZIONE_GUEST', 'nome_guest')
    cognome = config.get('SEZIONE_GUEST', 'cognome_guest')
    telefono = config.get('SEZIONE_GUEST', 'telefono_guest')
    time.sleep(2)

    carrello = driver.find_element_by_xpath(
        '/html/body/div[1]/div[1]/header/div/div[2]/div[2]/div/div/div/div[1]/a/span[3]/span[1]').text

    prodotto = str(random.randint(1,3))

    element_to_hover_over = driver.find_element_by_css_selector('#nav > li.level0.nav-3.level-top.parent')
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()

    driver.find_element_by_xpath('//*[@id="nav"]/li[3]/div/div/div/div['+ prodotto +']/a/img').click()
    time.sleep(3)

    d = 0
    size = [0 for x in range(d)]
    i = 0

    all_option = driver.find_elements_by_tag_name('div')
    for option in all_option:
        if option.get_attribute('class') == 'swatch-option text':
            lung = option.get_attribute('option-id')
            size.append(lung)
            d = d + 0
            i = i + 1

    lunghezza = (len(size))

    a = random.randint(0 , lunghezza-1)
    b = str(size[a])

    driver.find_element_by_xpath('//*[@id="option-label-lunghezza-152-item-' + b +'"]').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="product-addtocart-button"]').click()
    time.sleep(2)
    print("Ho aggiunto un prodotto al carrello")
    time.sleep(5)

    print("Dopo aver aggiunto un prodotto al carrello clicco sul mini-cart")
    driver.find_element_by_xpath('/html/body/div[1]/div/header/div/div[2]/div[2]/div/div/div/div[1]').click()
    time.sleep(5)

    print("Sto per cliccare su 'Vai al Checkout'")
    driver.find_element_by_xpath('//*[@id="top-cart-btn-checkout"]').click()
    time.sleep(5)

    try:
        assert "CHECKOUT" in driver.find_element_by_xpath('//*[@id="checkout"]/div[1]/h1').text
        print("Sono al Checkout")
    finally:
        time.sleep(3)

    print("Inserisco i dati per l'ordine")
    time.sleep(3)
    driver.find_element_by_id("customer-email").click()
    driver.find_element_by_id("customer-email").clear()
    driver.find_element_by_id("customer-email").send_keys(email)
    print("Ho inserito l'email")

    driver.find_element_by_name('firstname').click()
    driver.find_element_by_name('firstname').send_keys(nome)
    print("Ho inserito il nome")

    driver.find_element_by_name('lastname').click()
    driver.find_element_by_name('lastname').send_keys(cognome)
    print("Ho inserito il cognome")

    driver.find_element_by_name('street[0]').click()
    driver.find_element_by_name('street[0]').send_keys("Pedro Alvarez Cabrai, 16")
    print("Ho inserito l'indirizzo")

    driver.find_element_by_name('region').click()
    Select(driver.find_element_by_name("region")).select_by_visible_text("Cosenza")
    print("Ho selezionato la provincia")

    driver.find_element_by_name('city').click()
    Select(driver.find_element_by_name("city")).select_by_visible_text("Rende")
    print("Ho selezionato la città")

    driver.find_element_by_name('postcode').click()
    Select(driver.find_element_by_name("postcode")).select_by_visible_text("87036")
    print("Ho selezionato il CAP")

    driver.find_element_by_name('telephone').click()
    driver.find_element_by_name('telephone').send_keys(telefono)
    print("Ho inserito il numero di telefono")

    # driver.find_element_by_xpath("//div[9]/div/input").click()
    # driver.find_element_by_xpath("//div[9]/div/input").send_keys("FTTTST80A01C351A")
    # print("Ho inserito il codice fiscale")
    #
    # p_iva = random.randint(00000000001, 99999999999)
    #
    # driver.find_element_by_name("vat_id").click()
    # driver.find_element_by_name("vat_id").clear()
    # driver.find_element_by_name("vat_id").send_keys(p_iva)
    # print("Ho inserito la Partita Iva")

    print("Seleziono un metodo di pagamento")
    driver.find_element_by_xpath("//div[@id='checkout-payment-method-load']/div/div/div[3]/div/label").click()
    print("Ho selezionato 'Banca Sella'")
    time.sleep(5)

    print("Inserisco il numero della carta")
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").clear()
    driver.find_element_by_id("easynolo_bancasellapro_cc_number").send_keys("4775718800003024")

    print("Inserisco il mese di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration"]')).select_by_visible_text("05 - maggio")

    print("Inserisco l'anno di scadenza")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]').click()
    Select(driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_expiration_yr"]')).select_by_visible_text("2027")

    print("Inserisco il codice CVV")
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').click()
    driver.find_element_by_xpath('//*[@id="easynolo_bancasellapro_cc_cid"]').send_keys("646")

    print("Inserisco il nome cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_name").send_keys("Mario Rossi")

    print("Inserisco l'email cliente")
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").click()
    driver.find_element_by_id("easynolo_bancasellapro_cc_user_email").send_keys("mariorossi@future.com")

    print("Seleziono l'iscrizione alla newsletter")
    driver.find_element_by_xpath('//*[@id="checkout"]/div[4]/div[3]/div[3]/label').click()
    time.sleep(5)

    print("Inserisco un commento")
    driver.find_element_by_id("order_comments").click()
    driver.find_element_by_id("order_comments").send_keys("Test prova")
    time.sleep(3)

    print("Sto per effettuare l'ordine")
    driver.find_element_by_css_selector("span.title").click()
    time.sleep(3)

    try:
        assert "Payment transaction not authorized: Autorizzazione negata." in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div[2]/div[3]/div/div").text
        print("Pagamento negato")
    finally:
        time.sleep(3)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_checkout_guest_failed()
driver.quit()
print("END " + nomeTest)
