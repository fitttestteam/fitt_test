#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-011_RicercaRivenditore.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_ricerca_rivenditore():
    time.sleep(3)

    driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[4]/a').click()
    print("Ho cliccato su 'Trova Rivenditore")
    time.sleep(3)

    try:
        assert u"TROVA IL RIVENDITORE PIÙ VICINO A TE" in driver.find_element_by_xpath('//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div/p[1]').text
        print("Pagina 'Trova Rivenditore' caricata correttamente")
    finally:
        time.sleep(2)

    driver.find_element_by_id('stockist-search-term').click()
    driver.find_element_by_id('stockist-search-term').send_keys("88050")
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="stockists-submit"]').click()
    print("Ho effettuato la ricerca di un Rivenditore")



# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_ricerca_rivenditore()
driver.quit()
print("END "+nomeTest)
