#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-012_FAQ.py'
print('START '+nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

def test_faq():
    time.sleep(3)

    driver.find_element_by_xpath('//div/div/div/div[2]/nav/div/ul/li/a/span').click()
    time.sleep(5)
    print("ho cliccato su 'Domande e Risposte'")
    print("Verifico il corretto caricamento della pagina 'FAQ'")

    try:
        assert "FAQ" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento pagina 'FAQ' corretta")
    finally:
        time.sleep(2)

    assert u"SI PUÒ RIPARARE IL TUBO PER L'IRRIGAZIONE YOYO?" in driver.find_element_by_xpath('//*[@id="faq-id-7"]/a').text
    print("Controllo la presenza di una domanda OK")

# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_faq()
driver.quit()
print("END "+nomeTest)