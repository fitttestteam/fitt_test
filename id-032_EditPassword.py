#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-032_EditPassword.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_edit_password():

    email_1 = config.get('SEZIONE_REGISTRAZIONE', 'emailgen')
    password = config.get('DATI_REGISTRAZIONE', 'passwordreg')

    time.sleep(3)
    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la password utilizzata in fase di registrazione")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(password)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    print("Clicco sul nome per andare al mio account")
    driver.find_element_by_xpath("//span/span").click()
    time.sleep(3)
    driver.find_element_by_link_text("Il mio Account").click()

    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Caricamento 'Dashboard' corretto")
    finally:
        time.sleep(2)

    driver.find_element_by_link_text("Informazioni Account").click()
    print("Ho cliccato su 'Informazioni Account' dal menù sulla sinistra")

    casuale  = random.choice('abcdefghilmnopqrstuvz')
    cas = casuale.upper()
    print(cas)

    casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(6)]
    casuale_2 = ''.join(casuale_1)
    print(casuale_2)

    a = str(random.randint(1, 99))
    print(a)

    p_dopo = cas + casuale_2 + a
    print(p_dopo)

    config.set('DATI_REGISTRAZIONE', 'password_dopo', p_dopo)
    with open(filediconfigurazione, 'w') as configfile:
         config.write(configfile)

    driver.find_element_by_id("change-password").click()
    print("Ho selezionato 'Cambia Password'")

    driver.find_element_by_id("current-password").click()
    driver.find_element_by_id("current-password").send_keys(password)
    print("Ho inserito la Password Attuale")

    driver.find_element_by_id("password").click()
    driver.find_element_by_id("password").send_keys(p_dopo)
    print("Ho inserito la nuova Password")

    driver.find_element_by_id("password-confirmation").click()
    driver.find_element_by_id("password-confirmation").send_keys(p_dopo)
    print("Ho reinserito la nuova Password")

    driver.find_element_by_xpath('//*[@id="form-validate"]/div/div[1]/button').click()
    print("Ho cliccato su Salva")
    time.sleep(3)

    try:
        assert "Hai salvato le informazioni sull'account." in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div[2]/div[2]/div/div/div").text
        print("Modifica Password salvata correttamente")
    finally:
        time.sleep(3)

    print("Dopo aver salvato il cambio password esco ed effettuo una login con la nuova Password")
    driver.find_element_by_xpath("//span/span").click()
    driver.find_element_by_link_text("Esci").click()
    print("Ho effettuato la logout")
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_link_text("Accedi").click()
    time.sleep(3)

    print("Inserisco l'email")
    driver.find_element_by_id('email').click()
    driver.find_element_by_id('email').send_keys(email_1)
    time.sleep(3)
    print("Inserisco la nuova password")
    driver.find_element_by_id('pass').click()
    driver.find_element_by_id('pass').send_keys(p_dopo)
    time.sleep(3)

    print("Sto per cliccare su 'Accedi'")
    driver.find_element_by_xpath('//*[@id="send2"]').click()
    print("Ho cliccato su 'Accedi'")
    time.sleep(5)

    try:
        assert "La mia Bacheca" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div[2]/div/h1/span").text
        print("Login di prova effettuato con successo")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_edit_password()
driver.quit()
print("END " + nomeTest)