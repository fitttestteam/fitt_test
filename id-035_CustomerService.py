#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time, random
import re
import configparser

nomeTest = 'id-035_CustomerService.py'
print('START ' + nomeTest)

filediconfigurazione = '/home/fitt_test/ConfigFile.properties'
# filediconfigurazione = './ConfigFile.properties'

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)

# leggo il file di properties che contiene la password iniziale
config = configparser.RawConfigParser()
config.read(filediconfigurazione)

def test_customer_service():
    time.sleep(3)

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(5)

    try:
        assert "Newsletter" in driver.find_element_by_css_selector("div.footer-block-title > h5").text
        print("verifica caricamento modulo iscrizione 'Newsletter' OK")
    finally:
        time.sleep(3)

    try:
        assert "Chi siamo" in driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[1]/a').text
        print("verifica caricamento 'Chi siamo' OK")
    finally:
        time.sleep(2)

    try:
        assert "Metodi di pagamento" in driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[2]/a').text
        print("verifica caricamento 'Metodi di Pagamento' OK")
    finally:
        time.sleep(2)

    try:
        assert "Privacy Policy" in driver.find_element_by_xpath(
            '/html/body/div[1]/footer/div/div/div/div[1]/div/div[3]/div[2]/ul/li[1]/a').text
        print("verifica caricamento 'Privacy Policy' OK")
    finally:
        time.sleep(2)

    try:
        assert "Social" in driver.find_element_by_xpath(
            '/html/body/div[1]/footer/div/div/div/div[1]/div/div[4]/div[2]/div[2]/h5').text
        print("verifica caricamento modulo 'Social' OK")
    finally:
        time.sleep(2)

    print("Dopo aver visualizzato la presenza dei link provo a cliccare sulle pagine presenti nel SC 1.0")
    driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[2]/div[2]/ul/li[4]/a').click()
    print("Ho cliccato su 'Trova Rivenditore")
    time.sleep(3)

    try:
        assert u"TROVA IL RIVENDITORE PIÙ VICINO A TE" in driver.find_element_by_xpath(
            '//*[@id="maincontent"]/div/div/div/div[4]/div[2]/div/p[1]').text
        print("Pagina 'Trova Rivenditore' caricata correttamente")
    finally:
        time.sleep(2)

    print("Controllo la pagina 'Contatti'")
    driver.find_element_by_xpath('/html/body/div[1]/footer/div/div/div/div[1]/div/div[3]/div[2]/ul/li[4]/a').click()
    print("Ho cliccato su 'Contatti")
    time.sleep(3)

    print("Compilo i campi per inviare una richiesta")
    driver.find_element_by_id("name").click()
    driver.find_element_by_id("name").send_keys("Test")
    time.sleep(3)

    driver.find_element_by_id("city").click()
    driver.find_element_by_id("city").send_keys("Rende")
    time.sleep(3)

    email = config.get('EMAIL_RICHIESTA', 'email')

    driver.find_element_by_id("email").click()
    driver.find_element_by_id("email").send_keys(email)
    time.sleep(3)

    driver.find_element_by_id("comment").click()
    driver.find_element_by_id("comment").send_keys("Test Prova")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="form-contact-privacy-1"]').click()
    time.sleep(3)
    print("Ho cliccato sul trattamento dei dati per la richiesta di informazioni")

    driver.find_element_by_xpath('//*[@id="form-contact-privacy-2"]').click()
    time.sleep(3)
    print("Ho cliccato sul trattamento dei dati per il marketing")

    driver.find_element_by_xpath('//*[@id="contact-form"]/div/div/button/span').click()
    print("Ho cliccato su invia")
    time.sleep(2)

    try:
       assert u"Il Ticket è stato creato con successo" in driver.find_element_by_xpath("//main[@id='maincontent']/div/div/div/div[2]/div[2]/div/div/div").text
       print("Verifico chela richiesta sia stata inviata")
    finally:
        time.sleep(3)
        print("Vado ad aprire l'email per controllare aver ricevuto la conferma")

    url = config.get('EMAIL_RICHIESTA', 'url')

    driver.get(url)

    driver.find_element_by_xpath("//li/div/div[4]").click()
    print("Ho aperto l'email ricevuta")

    driver.switch_to.frame("msg_body")

    try:
        assert "Hello, Test.", driver.find_element_by_xpath("//strong").text
        print("Email ricevuta correttamente")
    finally:
        time.sleep(2)


# effettuo l'accesso
from Connection import Connection
Connection().connection(driver)

test_customer_service()
driver.quit()
print("END "+nomeTest)